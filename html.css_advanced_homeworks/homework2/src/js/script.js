const btn = document.querySelector(".toggle-button");
const nav = document.querySelector(".nav");

btn.addEventListener("click", function (e) {
  btn.classList.toggle("toggle-button--active");
  nav.classList.toggle("nav--active");
});
