/**
 * Задача 2.
 *
 * Написать функцию-исследователь навыков разработчика developerSkillInspector.
 *
 * Функция не обладает аргументами и не возвращает значение.
 * Функция опрашивает пользователя до тех пор, пока не введёт хотя-бы один свой навык.
 *
 * Если пользователь кликает по кнопке «Отменить» в диалоговом окне, программа оканчивает
 * опрос и выводит введённые пользователем навыки в консоль.
 *
 *
 * Условия:
 * - Список навыков хранить в форме массива;
 * - В списке навыков можно хранить только строки длиной больше, чем один символ.
 */

let developerSkillInspector = () => {
  let skills = [];
  let skill = null;
  while (skills.length == 0) {
    skill = prompt("Please enter your skill:");
    if (skill === null || skill === "" || skill.length < 2) {
      continue;
    } else {
      skills.push(skill);
    }
  }
  while (skills.length > 0) {
    skill = prompt("Please enter another skill:");
    if (skill === null) {
      break;
    } else if (skill.length < 2) {
      continue;
    } else if (skill !== null && skill !== "") {
      skills.push(skill);
      console.log(skills);
    }
  }
  console.log(skills);
};

developerSkillInspector();

// let developerSkillInspector = () => {
//   let skills = [];
//   let skill = null;
//   do {
//     skill = prompt("Введите навык:");
//     if (skill !== null && skill.length > 1) {
//       skills.push(skill);
//     }
//   } while (skills.length === 0 || skill !== null);
//   console.log(skills);
// };

// developerSkillInspector();
