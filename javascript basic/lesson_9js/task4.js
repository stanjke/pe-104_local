/**
 * Завдання 3.
 *
 * Поліпшити функцію-дослідник навичок розробника з попереднього завдання.
 *
 * Після ведення користувачем своїх навичок функція виводить їх на екран за допомогою функції alert.
 * Після чого запитує правильно, чи користувач ввів дані про свої навички.
 *
 * --- Якщо користувач відповів «ні» ---
 * Програма запитує його які навички необхідно видалити зі списку.
 * Якщо користувач ввів навичку для видалення зі списку, якого у списку не існує, програма
 * оповіщає користувача про помилку та повторно запитує дані.
 *
 * Програма запитує дані про навички для видалення зі списку до тих пір,
 * Поки користувач не клікне по кнопці «Скасувати» у діалоговому вікні.
 *
 * --- Якщо користувач відповів «так» ---
 * Програма виводить дані про навички в консоль.
 */

let developerSkillInspector = () => {
  let skills = [];
  let skill = null;
  do {
    skill = prompt("Введите навык:");
    if (skill !== null && skill.length > 1) {
      skills.push(skill);
    }
  } while (skills.length === 0 || skill !== null);
  let skillConfirm = confirm(`Are you sure you enter correct skills? "${skills}"`);
  console.log(skillConfirm); //false
  if (skillConfirm === false) {
    let skillToDelete = prompt("What skill you want to delete?");
    console.log(skillToDelete);
    let isInclude = skills.includes(skillToDelete); //false
    while (isInclude === false && skillToDelete !== null) {
      skillToDelete = prompt(`Skill ${skillToDelete} you want to delete is absent! Please enter correct skill:`);
      console.log(skillToDelete + "-" + typeof skillToDelete);
      isInclude = skills.includes(skillToDelete);
    }
    if (skillToDelete === null) {
      return console.log(skills);
    } else {
      skills.splice(skills.indexOf(skillToDelete), 1);
    }
  } else {
    return console.log(skills);
  }
  console.log(skills);
};

developerSkillInspector();
