/**
 * Задача 1.
 *
 * Написать функцию-помощник кладовщика replaceItems.
 *
 * Функция должна заменять указанный товар новыми товарами.
 *
 * Функция обладает двумя параметрами:
 * - Имя товара, который необходимо удалить;
 * - Список товаров, которыми необходимо заменить удалённый товар.
 *
 * Функция не обладает возвращаемым значением.
 *
 * Условия:
 * - Генерировать ошибку, если имя товара для удаления не присутствует в массиве;
 * - Генерировать ошибку, список товаров для замены удалённого товара не является массивом.
 *
 * Заметки:
 * - Дан «склад товаров» в виде массива с товарами через.
 */

/* Дано */
let storage = ["apple", "water", "banana", "pineapple", "tea", "cheese", "coffee"];

/* Решение */

let replaceItems = (deleteItem, insertitem) => {
  if (!Array.isArray(insertitem)) {
    throw `The second param is not Array, it's "${typeof insertitem}"!`;
  } else if (!storage.includes(deleteItem)) {
    throw `The first param is absent in stock!`;
  }
  storage.splice(deleteItem, 1, ...insertitem);
};

replaceItems("apple", ["pear", "banana", "bubble gum"]);
console.log(storage);
