/**
 * Задание 1.
 *
 * Написать скрипт, который создаст элемент button с текстом «Войти».
 *
 * При клике по кнопке выводить alert с сообщением: «Добро пожаловать!».
 */

 
 /**
  * Callback — это функция, которая срабатывает в ответ на событие.
  *
  * Совсем как в сервисом с функцией «перезвоните мне».
  * Мы оставляем заявку «перезвоните мне» — это событие.
  * Затем специалист на перезванивает (calls back).
  */
 
let button = document.createElement('input');
button.setAttribute('type', 'button');
button.value = 'Войти';
function clickHandler() {
    alert('Добро пожаловать!');
}
function hoverHandler() {
    alert('При клике по кнопке вы войдёте в систему.')
}

button.addEventListener('click', clickHandler);
button.addEventListener('mouseover', hoverHandler, {once: true})
document.body.append(button);

