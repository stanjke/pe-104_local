//------------------------//

// const student = {
//     name: "Stan",
//     "last name": "Ost",
//     laziness: 4,
//     trick: 5,
// }

//------------------------//

// const danItStudent = {};

// danItStudent.name =  "Stan";
// danItStudent.surname = "Ost";
// danItStudent["paste homeworks"] = 4;

// const property = prompt("Що хочете дізнатися про студента?");

// console.log(danItStudent[property]);

//------------------------//

// const product = {
//     name: null,
//     "full name": null,
//     price: 100,
//     availability: false,
//     "additional gift": null,
// }

// if (student.laziness >= 3 && student.laziness <= 5 && student.trick <= 4) {
//     console.log(`Студента ${student.name} ${student["last name"]} отправлен на перездачу`);
// }

//------------------------//

// const danItStudent = {
//     name: "Stan",
//     surname: "Ost",
//     homeworks: 10,
//   };

//   console.log(danItStudent);

//   let property = prompt("What property you want to change?");
//   let value = prompt("What value you want to setup for this property?");

//   danItStudent[property] = value;
//   console.log(danItStudent);

//------------------------//

// const danItStudent = {
//   name: "Stan",
//   surname: "Ost",
//   homeworks: 10,
// };
// console.log(danItStudent);

// for (const key in danItStudent) {
//   //   console.log(key); name
//   console.log(danItStudent[key]);
//   //   console.log(danItStudent); object {}
//   let property = prompt("What property you want to change?");
//   let newValue = prompt("What value you want to setup for this property?");
//   if (!property || !newValue) {
//     break;
//   }
//   if ((property = key)) {
//     danItStudent[key] = newValue;
//   }
// }

// for (let i = 0; i < 3; i++) {
//   let property = prompt("What property you want to change?");
//   let newValue = prompt("What value you want to setup for this property?");
//   if (!property || !newValue) {
//     break;
//   }

//   if (property === "homeworks") {
//     newValue = +newValue;
//   }
//   danItStudent[property] = newValue;
// }

// console.log(danItStudent);

//------------------------//

// const danItStudent = {
//   name: "Stan",
//   surname: "Ost",
//   homeworks: 10,
// };
// console.log(danItStudent);

// let property = prompt("What property you want to change?");

// while (!(property in danItStudent)) {
//   property = prompt(`${property} у об'єкта немає. Напишіть, будь ласка, правильну властивість об'єкта`);
//   if (property === null) {
//     break;
//   }
// }

// if (property !== null) {
//   let newValue = prompt("Яке значення ви хочете надати цій властивості?");
//   if (property === "homeworks") {
//     newValue = +newValue;
//   }
//   danItStudent[property] = newValue;
// }
// console.log(danItStudent);

//------------------------//

// const danItStudent = {
//   name: "Stan",
//   surname: "Ost",
//   homeworks: 11,
// };
// console.log(danItStudent);

// for (let i = 0; i < 3; i++) {
//   let property = prompt("What property you want to change?");

//   while (!(property in danItStudent)) {
//     if (property === null) {
//       break;
//     }
//     property = prompt(`${property} у об'єкта немає. Напишіть, будь ласка, правильну властивість об'єкта`);
//   }

//   if (property !== null) {
//     let newValue = prompt("Яке значення ви хочете надати цій властивості?");

//     if (property === "homeworks") {
//       newValue = +newValue;
//     }
//     danItStudent[property] = newValue;
//   } else {
//     break;
//   }
// }

// console.log(danItStudent);

//------------------------//

// const student = {
//   name: "Stan",
//   "last name": "Ost",
//   laziness: 4,
//   trick: 5,
// };

// if (student.laziness >= 3 && student.laziness <= 5 && student.trick >= 4) {
//   student["new status"] = `${student.name} ${student["last name"]} відправлено на перездачу`;
// }

// console.log(student);

//------------------------//

// const student = {};

// let studentName = prompt("Ваше ім'я");
// let studentSurname = prompt("Ваше прізвище");

// student.name = studentName;
// student.surname = studentSurname;

// console.log(`Студент ${student.name} ${student.surname} готовий до заповнення табеля!`);

// for (;;) {
//   let subject = prompt("Введіть назву предмета");
//   if (subject !== null) {
//     student[subject] = null;
//   } else {
//     break;
//   }
//   let mark = +prompt(`Введіть оцінку по предмету ${subject}`);
//   student[subject] = mark;
// }

// console.log(student);

//------------------------//

// const student = {
//   name: "Stan",
//   "last name": "Ost",
//   laziness: 5,
//   trick: 3,
//   course: 2,
// };

// console.log(student);

// if (student.laziness >= 5 && student.trick < 4) {
//   delete student.course;
//   student["new status"] = `Студент ${student.name} ${student["last name"]} направлено на перездачу`;
// }

//------------------------//

// const tabel = {
//   history: null,
//   biology: null,
//   javascript: null,
// };

// let average = 4;

// tabel.history = +prompt(`What's the mark of the subject history`);
// if (tabel.history < average) {
//   delete tabel.history;
// }

// tabel.biology = +prompt(`What's the mark of the subject biology`);
// if (tabel.biology < average) {
//   delete tabel.biology;
// }

// tabel.javascript = +prompt(`What's the mark of the subject javascript`);
// if (tabel.javascript < average) {
//   delete tabel.javascript;
// }

// console.log(tabel);

//------------------------//

// const user = {
//   name: "Влад",
//   "second name": "Дракула",
//   age: 400,
//   marriage: false,
// };

// let length = 0;
// for (let key in user) {
//   length++;
//   console.log(length);
// }

// user.length = length;
// console.log(user);

//------------------------//

// const casesList = {};

// for (;;) {
//   let jobName = prompt("Назва справи?", "programmer");
//   if (jobName !== null) {
//     casesList[jobName] = null;
//   } else {
//     break;
//   }
//   let timeSpent = +prompt("Затрачена кількість хвилин?", 60);
//   if (timeSpent !== null) {
//     casesList[jobName] = timeSpent / 60;
//   } else {
//     break;
//   }
//   //   if (jobName === null || timeSpent === null) {
//   //     break;
//   //   }
// }
// let length = 0;
// let timeSum = 0;

// for (let key in casesList) {
//   length++;
//   timeSum += casesList[key];
//   console.log(timeSum);
// }
// casesList.length = length;

// if (length > 3) {
//   console.log("Хтось оптимист!");
// }

// if (length > 3 && timeSum > 5) {
//   console.log("І дуже хоче бути схожим на Цезаря!");
// }

// console.log(casesList);
