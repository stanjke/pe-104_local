// CREATING CALC //

const calc = {
    value1: null,
    value2: null,

    read() {
        this.value1 = +prompt("Enter value 1 please");
        this.value2 = +prompt("Enter value 2 please");
    },
    sum() {
        return this.value1 + this.value2;
    },
    mul() {
        return this.value1 * this.value2;      
    }
}

calc.read();
console.log(calc.sum());
console.log(calc.mul());
console.log(calc["mul"]());