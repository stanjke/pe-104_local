const user = {
    firstName: "Jhon",
    lastName: "Brown",
    _age: 30,

    set age(value) {
        if (value > 0 && value < 120) {
            this._age = value;
        } else {
            alert("Please enter correct age!")
        }
    },
    
    get age() {
        return this._age;
    }
}

user.firstName = prompt("Enter your name");
user.age = +prompt("Enter your age");
console.log(user);