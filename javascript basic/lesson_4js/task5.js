/**
 * Задание 3.
 *
 * Написать функцию, которая возвращает наибольшее число,
 * из переданных ей в качестве аргументов при вызове.
 *
 * Генерировать ошибку, если:
 * - Если функция была вызвана менее, чем с
 * двумя аргументами;
 * - Хоть один из аргументов не является допустимым числом
 * (в ошибке указать его порядковый номер).
 *
 * Условия:
 * - Использовать тип функции arrow function;
 * - Использовать объект arguments запрещено;
 * - Обязательно использовать объект Math.
 */

let maxNum = (...rest) => {
  console.log(rest);
  if (rest.length < 2) {
    return console.error("Not enought arguments!");
  }
  let resault = [];
  //   let resault = rest[0];
  for (let i = 0; i < rest.length; i++) {
    if (isNaN(rest[i]) || typeof rest[i] !== "number") {
      console.error(`The argument ${i + 1} is not a number!`);
      continue;
    } else {
      resault.push(rest[i]);
    }
    // if (resault < rest[i]) {
    //   resault = rest[i];
    // }
  }
  return Math.max(...resault);
  //   return resault;
};

// let maxNum2 = (...rest) => {
//   let result = rest[0];
//   console.log(rest);
//   console.log(...rest);
//   result = Math.max(...rest);

//   return result;
// };

console.log(maxNum(12, "24", 34, 15, 21, 16, 152, "222"));
// console.log(maxNum2(12, "44", 34, 15, 21, 16));
// console.log(maxNum(12));
// console.log(maxNum(12, "24", 34));

// let arrayOfNumbers = [1, 43, 56, 22, 12, 33];
// console.log(Math.max(...arrayOfNumbers));
