// Ваша цель - создать функцию, которая удаляет первый и последний символы строки.
// Вам дается один параметр - исходная строка.

// Пример кода:
// removeChar('eloquent') => 'loquen'
// removeChar('country') => 'ountr'
// removeChar('person') =>'erso'
// removeChar('place') =>'lac'

function deleteSymbol(str) {
  let newStr = "";
  for (let i = 1; i < str.length - 1; i++) {
    newStr += str[i];
  }
  return newStr;
}

console.log(deleteSymbol("abalal"));
