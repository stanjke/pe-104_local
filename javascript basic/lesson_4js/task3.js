/**
 * Задание 2.
 *
 * Написать функцию-сумматор всех своих параметров.
 *
 * Функция принимает произвольное количество параметров.
 * Однако каждый из них обязательно должен быть числом.
 *
 * Генерировать ошибку, если:
 * - Если функция была вызвана менее, чем с двумя аргументами;
 * - Хоть один из аргументов не является допустимым числом (в ошибке указать его порядковый номер).
 *
 * Условия:
 * - Использовать тип функции arrow function;
 * - Использовать объект arguments запрещено.
 */


let sum = (...rest) => {
    let resault = 0;
    if (rest.length < 2) {
        console.error("Not enought arguments!");
        throw new Error("Not enought arguments!")
    }
    for (let i = 0; i < rest.length; i++) {
        // console.log(typeof rest[i]);
        if (isNaN(rest[i]) || typeof rest[i] !== "number") {
            console.error("Not a number");
            return;
        }
        // console.log(resault, rest[i]);
        resault = resault + rest[i]
    }
    return resault;
}

// console.log(sum(2, 4, 9));
console.log(sum(12, "24", 34, 15, 21, 16, 152, "222"));
// console.log(sum(2, 4, 123, true));
// console.log(sum(2));

