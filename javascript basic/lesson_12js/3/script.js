/**
 * Задание 3.
 *
 * Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.
 *
 * Цвет тёмных ячеек — #161619.
 * Цвет светлых ячеек — #FFFFFF.
 * Остальные стили CSS для доски и ячеек готовы.
 *
 * Доску необходимо развернуть внутри элемента с классом .board.
 *
 * Каждая ячейка доски представляет элемент div с классом .cell.
 */

/* Дано */
const LIGHT_CELL = "#ffffff";
const DARK_CELL = "#161619";
const V_CELLS = 8;
const H_CELLS = 8;

function fillChessBoard() {
  let board = document.createElement("section");
  board.classList.add("board");
  document.body.append(board);
  let isRowEven = true;
  for (let i = 1; i <= V_CELLS * H_CELLS; i++) {
    let cell = document.createElement("div");
    cell.classList.add("cell");
    console.log(isRowEven + ` при входе в цикл № ${i}`);
    if (i % 2 === 0) {
      console.log(isRowEven + ` при первой проверке если тру в цикле № ${i}`);
      cell.style.background = isRowEven ? LIGHT_CELL : DARK_CELL;
      board.append(cell);
    } else {
      console.log(isRowEven + ` при первой проверке если ФАЛС в цикле № ${i}`);
      cell.style.background = isRowEven ? DARK_CELL : LIGHT_CELL;
      board.append(cell);
    }
    if (i % H_CELLS === 0) {
      console.log(isRowEven + ` при последней проверке на четность ряда цикле № ${i}`);
      isRowEven = !isRowEven;
    }
  }
}

fillChessBoard();
