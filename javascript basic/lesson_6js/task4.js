const user = {
    firstName: "Stan",
    surname: "Ost",
    job: "student",
    hobbies: {
        sport: "football",
        collect: "coins",
        games: "chess"
    },
    sayHi() {
        console.log(`Привет, меня зовут ${this.firstName} ${this.surname}`);
    },
    updateProperty(key, value) {
        if (key in this) {
            this[key]= value;
        } else {
            throw new Error(`${key} is absent!`)
        }
    }
}

for (let key in user) {
    if (!isFunc(user[key]) && !isObj(user[key]))
    console.log(`Level 1 ${key} : ${user[key]} - ${typeof user[key]}`);
    if (isObj(user[key])) {
        console.log(`Level 1 ${key} :`);
        for (let key2 in user[key]) {
            console.log(`Level 2 ${key2} : ${user[key][key2]} - ${typeof user[key][key2]}`);
        }
    }
}

function isFunc(value) {
    if (typeof value === "function") {
        return true;
    }
}

function isObj(value) {
    if (typeof value === "object")
    return true;
}