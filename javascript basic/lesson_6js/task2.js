let firstName = "Stan";

const user = {
    firstName,
    surname: "Ost",
    job: "student",
    sayHi() {
        console.log(`Привет, меня зовут ${this.firstName} ${this.surname}`);
    },
    updateProperty(key, value) {
        if (key in this) {
            this[key]= value;
        } else {
            throw new Error(`${key} is absent!`)
        }
    }
}
console.log(user);
user.sayHi();
user.updateProperty("surname", "Tom")
console.log(user);