let firstName = "Stan";

const user = {
    firstName,
    surname: "Ost",
    job: "student",
    sayHi() {
        console.log(`Привет, меня зовут ${this.firstName} ${this.surname}`);
    },
    updateProperty(key, value) {
        if (key in this) {
            this[key]= value;
        } else {
            throw new Error(`${key} is absent!`)
        }
    },
    addingProperty(key, value) {
        if (this.hasOwnProperty(key)) {
            throw new Error(`${key} is already exist!`)
        } else {
            this[key] = value;
        }
    }
    // addingProperty(key, value) {
    //     if (key in this) {
    //         throw new Error(`${key} is already exist!`)
    //     } else {
    //         this[key] = value;
    //     }
    // }
}
console.log(user);
user.sayHi();
user.updateProperty("surname", "Tom")
console.log(user);
user.addingProperty("salary", "2000")
user.addingProperty("surname", "Tom")