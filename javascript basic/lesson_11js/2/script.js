/**
 * Задание 2.
 *
 * Написать функцию-фабрику квадратов createSquares.
 *
 * Функция обладает двумя параметром — количеством квадратов для создания.
 *
 * Если пользователь ввёл количество квадратов для создания в недопустимом формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Максимальное количество квадратов для создания — 10.
 * Если пользователь решил создать более 10-ти квадратов — сообщить ему о невозможности такой операции
 * и запрашивать данные о количестве квадратов для создания до тех пор, пока они не будут введены корректно.
 *
 * Размер каждого квадрата в пикселях нужно получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в недопустимом формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Цвет каждого квадрата необходимо запросить после введения размера квадрата в корректном виде.
 *
 * Итого последовательность ввода данных о квадратах выглядит следующим образом:
 * - Размер квадрата n;
 * - Цвет квадрата n;
 * - Размер квадрата n + 1;
 * - Цвет квадрата n + 1.
 * - Размер квадрата n + 2;
 * - Цвет квадрата n + 3;
 * - и так далее...
 *
 * Если не любом этапе сбора данных о квадратах пользователь кликнул по кнопке «Отмена»,
 * необходимо остановить процесс создания квадратов и вывести в консоль сообщение:
 * «Операция прервана пользователем.».
 *
 * Все стили для каждого квадрата задать через JavaScript за раз.
 *
 * Тип элемента, описывающего каждый квадрат — div.
 * Задать ново-созданным элементам CSS-классы: .square-1, .square-2, .square-3 и так далее.
 *
 * Все квадраты необходимо сделать потомками body документа.
 */

let inputAmount = +prompt("Please enter the number of squares:");

while (isNaN(inputAmount) || inputAmount > 10) {
  if (isNaN(inputAmount)) {
    inputAmount = +prompt("Please enter the number!");
  } else if (inputAmount > 10) {
    inputAmount = +prompt("Please enter the number less then 10!");
  }
}

function createSquares(amount) {
  let squares = new Array(amount).fill(null).map((element, index) => {
    let square = document.createElement("div");
    square.classList.add(`square-${index + 1}`);
    let inputSize = +prompt(`Please enter size for your square! It's your ${index + 1} square`);
    while (isNaN(inputSize)) {
      inputSize = +prompt("Please enter correct size!");
    }
    let inputColor = prompt(`Please enter the color for your suqare number ${index + 1}`);
    square.style.cssText = `
	width: ${inputSize}px;
	height: ${inputSize}px;
	background: ${inputColor};
	`;
    return square;
  });
  squares.forEach((elem) => document.body.append(elem));
}

createSquares(inputAmount);
