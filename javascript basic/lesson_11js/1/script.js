/**
 * Задание 1.
 *
 * Написать скрипт, который создаст квадрат произвольного размера.
 *
 * Размер квадрата в пикселях получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в некорректном формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Все стили для квадрата задать через JavaScript посредством одной строки кода.
 *
 * Тип элемента, описывающего квадрат — div.
 * Задать ново-созданному элементу CSS-класс .square.
 *
 * Квадрат в виде стилизированного элемента div необходимо
 * сделать первым и единственным потомком body документа.
 */
let inputSize = +prompt("Please enter size of your square");

while(isNaN(inputSize)) {
        inputSize = +prompt("Pleases enter correct type!")
}

function squartCreate(size) {
    let div = document.createElement('div');
    div.classList.add('square');
    // div.style.background = 'orange';
    // div.style.width = size + "px";
    // div.style.height = size + "px";
    div.style.cssText = `
    width: ${size}px;
    height: ${size}px;
    background: orange;`;
    document.body.append(div);
}

squartCreate(inputSize);