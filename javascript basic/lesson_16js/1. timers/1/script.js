/**
 * Задание 1.
 *
 * Написать программу для напоминаний.
 *
 * Все модальные окна реализовать через alert.
 *
 * Условия:
 * - Если пользователь не ввёл сообщение для напоминания — вывести alert с сообщением «Ведите текст напоминания.»;
 * - Если пользователь не ввёл значение секунд,через сколько нужно вывести напоминание —
 *   вывести alert с сообщением «Время задержки должно быть больше одной секунды.»;
 * - Если все данные введены верно, при клике по кнопке «Напомнить» необходимо её блокировать так,
 *   чтобы повторный клик стал возможен после полного завершения текущего напоминания;
 * - После этого вернуть изначальные значения обоих полей;
 * - Создавать напоминание, если внутри одного из двух элементов input нажать клавишу Enter;
 * - После загрузки страницы установить фокус в текстовый input.
 */

const reminder = document.querySelector('#reminder');
const seconds = document.querySelector('#seconds');
const btn = document.querySelector('button');
let secondsValue = null;

btn.addEventListener('click', remind);
reminder.addEventListener('keydown', remindOnEnter);
seconds.addEventListener('keydown', remindOnEnter);

function remindOnEnter(event) {
    if(event.key === "Enter" && !btn.disabled) {
        remind();
    }
}

function remind() {
    let reminderValue = reminder.value;
    let secondsValue = parseInt(seconds.value);
    if(reminderValue.trim().length === 0) {
        alert('Please enter reminder!');
        return;
    }
    if(secondsValue < 1) {
        alert('Please enter correct time (more then 1 second)')
        return;
    }
    let intervalId = setInterval(() => {
        seconds.value = --secondsValue
    }, 1000)

    btn.setAttribute('disabled', 'true');
    setTimeout(() => {
        alert(reminderValue)
        btn.removeAttribute('disabled');
        reminder.value = '';
        seconds.value = 0; 
        clearInterval(intervalId)
    }, secondsValue * 1000);
}


