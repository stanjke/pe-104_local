/**
 * Задание 2.
 *
 * Написать программу, которая будет приветствовать пользователя.
 * Сперва пользователь вводит своё имя, после чего программа выводит в консоль сообщение 
 * с учётом его должности.
 *
 * Список должностей:
 * Mike — CEO;
 * Jane — CTO;
 * Walter — программист:
 * Oliver — менеджер;
 * John — уборщик.
 *
 * Если введёно не известное программе имя — вывести в консоль сообщение «Пользователь не найден.».
 *
 * Выполнить задачу в двух вариантах:
 * - используя конструкцию if/else if/else;
 * - используя конструкцию switch.
 */
/* if/else if/else */

const user = prompt("Enter your name, please!");

if (user === "Mike") {
    console.log(`Mike — CEO`);
} else if (user == 'Jane') {
    console.log('Jane — CTO');
} else if (user == 'Walter') {
    console.log('Walter — программист');
} else if (user == 'Oliver') {
    console.log('Oliver — менеджер');
} else if (user == 'John') {
    console.log('John — уборщик');
} else {
    console.log("Пользователь не найден.");
}

const user = prompt("Enter your name, please!");

switch (user) {
    case "Mike":
    console.log(`Mike — CEO`);
    break;
    case "Jane":
    console.log('Jane — CTO');
    break;
    case "Walter":
    console.log('Walter — программист');
    break;
    case "Oliver":
    console.log('Oliver — менеджер');
    break;
    case "John":
    console.log('John — уборщик');
    break;
    default:
        console.log("Пользователь не найден.")
} 