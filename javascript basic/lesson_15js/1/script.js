/**
 * Задача 1.
 *
 * Необходимо «оживить» навигационное меню с помощью JavaScript.
 *
 * При клике на элемент меню добавлять к нему CSS-класс .active.
 * Если такой класс уже существует на другом элементе меню, необходимо
 * с того, предыдущего элемента CSS-класс .active снять.
 *
 * У каждый элемент меню — это ссылка, ведущая на google.
 * С помощью JavaScript необходимо предотвратить переход по всем ссылка на этот внешний ресурс.
 *
 * Условия:
 * - В реализации обязательно использовать приём делегирования событий (на весь скрипт слушатель должен быть один).
 */

let ulList = document.querySelector('ul');
let links = document.querySelectorAll('a');
console.log(links);
ulList.addEventListener('click', function(event){
    event.preventDefault();
    let closestA = event.target.closest('a');
    if(closestA.classList.contains('active')) {
        closestA.classList.remove('active')
        return;
    } else {
        for(link of links) {
            if(link.classList.contains('active')){
                link.classList.remove('active');
            }
        }
        closestA.classList.add('active');
    }



})