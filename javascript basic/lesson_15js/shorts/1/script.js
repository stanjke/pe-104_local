/**
 * Создайте дерево, которое по клику на заголовок скрывает-показывает потомков:
 *
 * 
 * Условия:
 * - Использовать только один обработчик событий (применить делегирование)
 * Клик вне текста заголовка (на пустом месте) ничего делать не должен.
 */
let treeList = document.querySelector('#tree');

treeList.addEventListener('click', function(event){
    let childrenContainer = event.target.querySelector('ul');
    if(childrenContainer == null){
        return;
    } 
    childrenContainer.hidden = !childrenContainer.hidden;
    
    console.log(childrenContainer);
})