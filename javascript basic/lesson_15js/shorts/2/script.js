/**
 * Сделайте так, чтобы при клике на ссылки внутри элемента id="contents" пользователю выводился вопрос о том, 
 * действительно ли он хочет покинуть страницу, и если он не хочет, то прерывать переход по ссылке.
 *
 * 
 * Условия:
 * Содержимое #contents может быть загружено динамически и присвоено при помощи innerHTML. 
 * Так что найти все ссылки и поставить на них обработчики нельзя. Используйте делегирование.
 * Содержимое может иметь вложенные теги, в том числе внутри ссылок, например, <a href=".."><i>...</i></a>.
 */

let contents = document.querySelector('#contents');

contents.addEventListener('click', function(event){
    event.preventDefault();

    let target = event.target.closest('a');
    if(target) {
        let isLeave = confirm('Are you sure you want to leave the page?');
        if(isLeave) {
            location.href = target.getAttribute('href');
        } else {

        }

    }
})