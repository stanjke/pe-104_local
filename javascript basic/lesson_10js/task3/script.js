/**
 * Задание 4.
 *
 * Написать программа для редактирования количества остатка продуктов на складе магазина.
 *
 * Программа должна запрашивать название товара для редактирования.
 * Если ввёденного товара на складе нет — программа проводит повторный запрос названия товара
 * до тех пор, пока соответствующее название не будет введено.
 *
 * После чего программа запрашивает новое количество товара.
 * После чего программа вносит изменения на веб-страницу: заменяет остаток указанного товара его новым количеством.
 */

let storeUl = document.querySelector(".store").children;
let inputProduct = prompt("Please enter product you want to change:");
let storeObj = new Map();

for (let valueLi of storeUl) {
  let productArray = valueLi.innerHTML.split(": ");
  storeObj.set(productArray[0], productArray[1]);
}

while (!storeObj.has(inputProduct)) {
  inputProduct = prompt("Please enter correct product:");
}

for (let valueLi of storeUl) {
  console.log(valueLi);
  let productArray = valueLi.innerHTML.split(": ");
  if (inputProduct === productArray[0]) {
    let inputAmount = prompt("Please enter new amount:");
    productArray[1] = inputAmount;
    valueLi.innerHTML = productArray.join(": ");
  }
}
