let links = document.body.querySelectorAll("li>a");

console.log(links);

for (let i of links) {
  if (i.href.startsWith("http://internal.com") || i.href.includes("127.0.0.1")) {
    continue;
  }
  if (i.href.includes("://")) {
    i.style.color = "orange";
  }
}
