/**
 * Задание 1.
 *
 * Написать имплементацию встроенной функции строки repeat(times).
 *
 * Функция должна обладать двумя параметрами:
 * - Целевая строка для поиска символа по индексу;
 * - Количество повторений целевой строки.
 *
 * Функция должна возвращать преобразованную строку.
 *
 * Условия:
 * - Генерировать ошибку, если первый параметр не является строкой, а второй не является числом.
 */


/* Решение */



let repeat = function(string, times) {
    let newStr = "";
    if (typeof string !== "string" || typeof times !== "number") {
        throw new Error("Error")
    }
     else {
        for(let i = 0; i < times; i++) {
            newStr += " " + string;
    }
}
    return newStr.trim();
    };
repeat("hello", 4)

console.log(repeat("hello", 10));
// console.log(repeat("hello", "hello"));
console.log(repeat(10, 10));
