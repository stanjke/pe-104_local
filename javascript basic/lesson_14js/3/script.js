/**
 * При натисканні shift та "+"" одночасно збільшувати
 * шрифт сторінки на 1px
 * а при shift та "-" - зменшувати на 1px
 *
 * Максимальний розмір шрифту - 30px, мінімальний - 10px
 *
 */

let fontSize = parseInt(window.getComputedStyle(document.body).fontSize);
console.log(fontSize);
document.addEventListener('keydown', function(event){
    console.log();
    if (fontSize > event.code == 'Equal' && event.shiftKey) {
        fontSize++
    }
    if (event.code == 'Minus' && event.shiftKey) {
        fontSize--
    }
    document.body.style.fontSize = fontSize + "px";
}) 