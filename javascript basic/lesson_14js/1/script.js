/**
 * Задание 1.
 *
 * Создать элемент h1 с текстом «Нажмите любую клавишу.».
 *
 * При нажатии любой клавиши клавиатуры менять текст элемента h1 на:
 * «Нажатая клавиша: ИМЯ_КЛАВИШИ».
 */

let text = document.createElement('h1');
text.innerText = 'Нажмите любую клавишу.';

document.body.append(text);

document.addEventListener('keydown', function(event) {
    text.innerText = `Нажатая клавиша: ${event.key}`
})
