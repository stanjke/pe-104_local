const product = {
  title: "Some text",
  cost: 500,
  "expiration date": "forever",
  getExpirationDate() {
    return product["expiration date"];
  },
  obj: {
    value1: 12,
    value2: "some text",
  },
};

//Так не работает. Мы скопировали только ссылку на объект, по этому все что мы будем менять
//для product2 так же будет меняться и для product..
// const product2 = product;
// product2.title = "Super good skills";
// console.log(product.title);
// console.log(product2.title);

//var 1. How to copy object into object.
//with "for in loop"
// const product2 = {};

// for (const key in product) {
//   product2[key] = product[key];
// }

// product2.title = "Super good skills";
// console.log(product.title);
// console.log(product2.title);
// console.log(product);
// console.log(product2);

//var2. With global object methood.
// const product2 = Object.assign({}, product);

// ---------------- //

console.log(Object.assign({ name: 3 }, { name: 1 }));
