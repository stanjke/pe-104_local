// let array = [1, [2, [3, 4], 5], 6];

// function flattenArray(arr) {
//   let result = [];
//   arr.forEach(elem => {
//     if (Array.isArray(elem)) {
//       result = result.concat(flattenArray(elem));
//     } else {
//       result.push(elem)
//     }
//   })
//   console.log(result);
//   return result;
// }


// flattenArray(array); // [1, 2, 3, 4, 5, 6]

// Напишите функцию, которая принимает на вход два массива и возвращает новый массив,
// содержащий элементы, которые есть только в одном из массивов.

let array1 = [1,2,3,4,5];
let array2 = [2,1,4,7,6,5,8];

function uniqArray(arr1, arr2) {
  arr2.forEach(elem => {
    if (!arr1.includes(elem)) {
      arr1.push(elem);
    }
  })
  return arr1;
}

console.log(uniqArray(array1,array2));;