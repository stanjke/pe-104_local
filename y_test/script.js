// const Numb = 111;
// const Str = "hellow world";
// const Bool = true;
// const myNull = null;
// const undef = undefined;

// const obj = {name: "Stanislav"};
// const arr = [];
// function fnName() {}


// console.log(typeof Numb); //number
// console.log(typeof Str);  //string
// console.log(typeof Bool);   //Boolean
// console.log(typeof myNull); //object
// console.log(typeof undef);  //undefind

// console.log(typeof obj);  //obj 
// console.log(typeof arr);  //obj
// console.log(typeof fnName);  //obj 


// alert("hello");

const num = 3;
const one = 1;
const zero = 0;
const str = "4";
const str2 = "asdf"
const emtStr = "";
const arr = [1,2,3];
const emtArr = [];
const obj = {age:18};
const emtObj = {};
const nL = null;
const nN = NaN;
const undef = undefined;

//ЯВНИЙ КАСТИНГ ПРЕОБРАЗОВАНИЕ В СТРОКУ
console.log("ЯВНИЙ КАСТИНГ ПРЕОБРАЗОВАНИЕ В СТРОКУ");

//methood String(value)   
console.log();
console.log("//methood String(value)");  
console.log(String(num), typeof String(num));                    //3 string
console.log(String(one), typeof String(one));                    //1 string
console.log(String(zero), typeof String(zero));                  //0 string
console.log(String(str), typeof String(str));                    //4 string
console.log(String(str2), typeof String(str2));                  //asdf string
console.log(String(emtStr), typeof String(emtStr));              //<empty string> string
console.log(String(arr), typeof String(arr));                    //1,2,3 string
console.log(String(emtArr), typeof String(emtArr));              //<empty string> string
console.log(String(obj), typeof String(obj));                    //[object Object] string
console.log(String(emtObj), typeof String(emtObj));              //[object Object] string
console.log(String(nL), typeof String(nL));                      //null string
console.log(String(nN), typeof String(nN));                      //NaN string
console.log(String(undefined), typeof String(undefined));        //undefined string
console.log();
console.log();

//methood value.toString
console.log();
console.log("//methood value.toString"); 
console.log((num).toString(), typeof (num).toString());                 //3 string 
console.log((str).toString(), typeof (str).toString());                 //4 string
console.log((str2).toString(), typeof (str2).toString());               //asdf string
console.log((emtStr).toString(), typeof (emtStr).toString());           //<empty string> string
console.log((arr).toString(), typeof (arr).toString());                 //1,2,3 string
console.log((emtArr).toString(), typeof (emtArr).toString());           //<empty string> string
console.log((obj).toString(), typeof (obj).toString());                 //[object Object] string
console.log((emtObj).toString(), typeof (emtObj).toString());           //[object Object] string
// console.log((nL).toString(), typeof (nL).toString());                //error - нету метода .toString
console.log((nN).toString(), typeof (nN).toString());                   //NaN string
// console.log((undefined).toString(), typeof (undefined).toString());  //error - нету метода .toString
console.log();
console.log();

//НЕ ЯВНИЙ КАСТИНГ - concat "+"
console.log("//НЕ ЯВНИЙ КАСТИНГ - concat +"); 
console.log("3" + 4); // 34
console.log("3" + {}); // 3[object Object]
console.log();
console.log();

// ---------------------
//ЯВНИЙ КАСТИНГ ПРЕОБРАЗОВАНИЕ В ЧИСЛО
console.log("ЯВНИЙ КАСТИНГ ПРЕОБРАЗОВАНИЕ В ЧИСЛО");
console.log();
console.log();

console.log("//methood Number(value)");
console.log(Number(num), typeof Number(num));                //3 number
console.log(Number(one), typeof Number(one));                //1 number
console.log(Number(zero), typeof Number(zero));              //0 number
console.log(Number(str), typeof Number(str));                //4 number
console.log(Number(str2), typeof Number(str2));              //NaN number
console.log(Number(emtStr), typeof Number(emtStr));          //0 number
console.log(Number(arr), typeof Number(arr));                //NaN number
console.log(Number(emtArr), typeof Number(emtArr));          //0 number
console.log(Number(obj), typeof Number(obj));                //NaN number
console.log(Number(emtObj), typeof Number(emtObj));          //NaN number
console.log(Number(nL), typeof Number(nL));                  //0 number
console.log(Number(nN), typeof Number(nN));                  //NaN number
console.log(Number(undefined), typeof Number(undefined));    //NaN number
console.log();
console.log();

console.log("//methood parseInt(value)");
console.log(parseInt(num), typeof parseInt(num));                //3 number
console.log(parseInt(one), typeof parseInt(one));                //1 number
console.log(parseInt(zero), typeof parseInt(zero));              //0 number
console.log(parseInt(str), typeof parseInt(str));                //4 number
console.log(parseInt(str2), typeof parseInt(str2));              //NaN number
console.log(parseInt(emtStr), typeof parseInt(emtStr));          //NaN number
console.log(parseInt(arr), typeof parseInt(arr));                //1 number
console.log(parseInt(emtArr), typeof parseInt(emtArr));          //NaN number
console.log(parseInt(obj), typeof parseInt(obj));                //NaN number
console.log(parseInt(emtObj), typeof parseInt(emtObj));          //NaN number
console.log(parseInt(nL), typeof parseInt(nL));                  //NaN number
console.log(parseInt(nN), typeof parseInt(nN));                  //NaN number
console.log(parseInt(undefined), typeof parseInt(undefined));    //NaN number
console.log();
console.log();

console.log("//methood parseFloat(value)");
console.log(parseFloat(num), typeof parseFloat(num));                //3 number
console.log(parseFloat(one), typeof parseFloat(one));                //1 number
console.log(parseFloat(zero), typeof parseFloat(zero));              //0 number
console.log(parseFloat(str), typeof parseFloat(str));                //4 number
console.log(parseFloat(str2), typeof parseFloat(str2));              //NaN number
console.log(parseFloat(emtStr), typeof parseFloat(emtStr));          //NaN number
console.log(parseFloat(arr), typeof parseFloat(arr));                //1 number
console.log(parseFloat(emtArr), typeof parseFloat(emtArr));          //NaN number
console.log(parseFloat(obj), typeof parseFloat(obj));                //NaN number
console.log(parseFloat(emtObj), typeof parseFloat(emtObj));          //NaN number
console.log(parseFloat(nL), typeof parseFloat(nL));                  //NaN number
console.log(parseFloat(nN), typeof parseFloat(nN));                  //NaN number
console.log(parseFloat(undefined), typeof parseFloat(undefined));    //NaN number
console.log();
console.log();

console.log("//methood unar +(value)");
console.log(+(num), typeof +(num));                //3 number
console.log(+(one), typeof +(one));                //1 number
console.log(+(zero), typeof +(zero));              //0 number
console.log(+(str), typeof +(str));                //4 number
console.log(+(str2), typeof +(str2));              //NaN number
console.log(+(emtStr), typeof +(emtStr));          //0 number
console.log(+(arr), typeof +(arr));                //NaN number
console.log(+(emtArr), typeof +(emtArr));          //0 number
console.log(+(obj), typeof +(obj));                //NaN number
console.log(+(emtObj), typeof +(emtObj));          //NaN number
console.log(+(nL), typeof +(nL));                  //0 number
console.log(+(nN), typeof +(nN));                  //NaN number
console.log(+(undefined), typeof +(undefined));    //NaN number
console.log();
console.log();


// ------------------
//ЯВНИЙ КАСТИНГ ПРЕОБРАЗОВАНИЕ В ЛОГИЧЕСКИЙ ТИП
console.log("ЯВНИЙ КАСТИНГ ПРЕОБРАЗОВАНИЕ В ЛОГИЧЕСКИЙ ТИП");
console.log();
console.log();

console.log("//methood Boolean(value)");
console.log(Boolean(num), typeof Boolean(num));                //true
console.log(Boolean(one), typeof Boolean(one));                //true
console.log(Boolean(zero), typeof Boolean(zero));              //false
console.log(Boolean(str), typeof Boolean(str));                //true
console.log(Boolean(str2), typeof Boolean(str2));              //true
console.log(Boolean(emtStr), typeof Boolean(emtStr));          //false
console.log(Boolean(arr), typeof Boolean(arr));                //true
console.log(Boolean(emtArr), typeof Boolean(emtArr));          //true
console.log(Boolean(obj), typeof Boolean(obj));                //true
console.log(Boolean(emtObj), typeof Boolean(emtObj));          //true
console.log(Boolean(nL), typeof Boolean(nL));                  //false
console.log(Boolean(nN), typeof Boolean(nN));                  //false
console.log(Boolean(undefined), typeof Boolean(undefined));    //false
console.log();
console.log();

console.log("//methood !(value)");
console.log(!(num), typeof !(num));                //false
console.log(!(one), typeof !(one));                //false
console.log(!(zero), typeof !(zero));              //true
console.log(!(str), typeof !(str));                //false
console.log(!(str2), typeof !(str2));              //false
console.log(!(emtStr), typeof !(emtStr));          //true
console.log(!(arr), typeof !(arr));                //false
console.log(!(emtArr), typeof !(emtArr));          //false
console.log(!(obj), typeof !(obj));                //false
console.log(!(emtObj), typeof !(emtObj));          //false
console.log(!(nL), typeof !(nL));                  //true
console.log(!(nN), typeof !(nN));                  //true
console.log(!(undefined), typeof !(undefined));    //true
console.log();
console.log();

console.log("//methood !!(value)");
console.log(!!(num), typeof !!(num));                //true
console.log(!!(one), typeof !!(one));                //true
console.log(!!(zero), typeof !!(zero));              //false
console.log(!!(str), typeof !!(str));                //true
console.log(!!(str2), typeof !!(str2));              //true
console.log(!!(emtStr), typeof !!(emtStr));          //false
console.log(!!(arr), typeof !!(arr));                //true
console.log(!!(emtArr), typeof !!(emtArr));          //true
console.log(!!(obj), typeof !!(obj));                //true
console.log(!!(emtObj), typeof !!(emtObj));          //true
console.log(!!(nL), typeof !!(nL));                  //false
console.log(!!(nN), typeof !!(nN));                  //false
console.log(!!(undefined), typeof !!(undefined));    //false

console.log(one || num);