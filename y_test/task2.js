let currentYear = new Date().getFullYear();

let firstName = prompt("Please enter your First name:");
console.log(firstName + " " + typeof firstName);

while (!isNaN(firstName) || firstName == "") {
  console.log(firstName + " " + typeof firstName);
  firstName = prompt("Please re-enter your First name:");
}

let lastName = prompt("Please enter your Last name:");

while (!isNaN(lastName) || lastName == "") {
  lastName = prompt("Please enter your Last name:");
}

let dateOfBirth = +prompt("Please enter year of your birthday:");
//
while (isNaN(dateOfBirth) || dateOfBirth < 1910 || dateOfBirth > currentYear) {
  dateOfBirth = +prompt("Please re-enter year of your birthday:");
}

console.log(`Добро пожаловать, родившийся в ${dateOfBirth}, ${firstName} ${lastName}.`);

// let string = "abc";
// console.log(string + " " + typeof string + " and " + Number(string) + " " + typeof Number(string) + " and " + isNaN(string) + " " + typeof isNaN(string));

// let emptString = "";
// console.log(emptString + " " + typeof emptString + " and " + Number(emptString) + " " + typeof Number(emptString) + " and " + isNaN(emptString) + " " + typeof isNaN(emptString));

// let numberMoreThenZero = 1;
// console.log(numberMoreThenZero + " " + typeof numberMoreThenZero + " and " + Number(numberMoreThenZero) + " " + typeof Number(numberMoreThenZero) + " and " + isNaN(numberMoreThenZero) + " " + typeof isNaN(numberMoreThenZero));

// let numberZero = 0;
// console.log(numberZero + " " + typeof numberZero + " and " + Number(numberZero) + " " + typeof Number(numberZero) + " and " + isNaN(numberZero) + " " + typeof isNaN(numberZero));
