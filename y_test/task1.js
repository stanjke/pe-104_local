"use strict";


// via while loop

let num1 = +prompt("Please enter number 1");
console.log(num1 + " " + typeof(num1));
while (isNaN(num1) || num1 == "") {
    console.log("this is not a number, please enter number..");
    num1 = +prompt("Please enter number 1!")
}


let num2 = +prompt("Please enter number 2");
console.log(num2 + " " + typeof(num2));
while (isNaN(num2) || num2 == "") {
    console.log("this is not a number, please enter number..");
    num2 = +prompt("Please enter number 2!")
}


// via for loop

let number1;

for (let input = 0; ;input++) {
    number1 = +prompt("Please enter number 1");
    if (isNaN(number1) || number1 == "") {
        number1 = +prompt("Please enter number 1")
    } else {
        break;
    }
}

console.log(number1);
