while (true) {
  let firstName = prompt("What's the student first name?", "Stan");
  let lastName = prompt("What's the student last name?", "Ost");
  let mark = +prompt("What's the student mark?");

  if (mark >= 0 && mark <= 100) {
    let convertToLetter;
    if (mark >= 95 && mark <= 100) {
      let repeater;
      convertToLetter = "A";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 90 && mark <= 94) {
      let repeater;
      convertToLetter = "A-";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 85 && mark <= 89) {
      let repeater;
      convertToLetter = "B+";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 80 && mark <= 84) {
      let repeater;
      convertToLetter = "B";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 75 && mark <= 79) {
      let repeater;
      convertToLetter = "B-";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 70 && mark <= 74) {
      let repeater;
      convertToLetter = "C+";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 65 && mark <= 69) {
      let repeater;
      convertToLetter = "C";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 60 && mark <= 64) {
      let repeater;
      convertToLetter = "C-";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 55 && mark <= 59) {
      let repeater;
      convertToLetter = "D+";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 50 && mark <= 54) {
      let repeater;
      convertToLetter = "D";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 25 && mark <= 49) {
      let repeater;
      convertToLetter = "E";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    } else if (mark >= 0 && mark <= 24) {
      let repeater;
      convertToLetter = "F";
      console.log(`К студенту ${firstName} ${lastName} прикреплена оценка ${convertToLetter}`);
      while (true) {
        repeater = prompt("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?", "Нет.");
        if (repeater == "Нет.") {
          console.log("✅ Работа завершена.");
          break;
        } else {
          console.log("Eсть-ли необходимость сконвертировать оценку для ещё одного студента?");
        }
      }
      break;
    }
  } else {
    console.log("Please enter correct info!");
  }
}
