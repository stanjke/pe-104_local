import { getIp, getInfo } from "./requests/index.js"


const btn = document.querySelector('#btn');

btn.addEventListener('click', async () => {
    const { ip } = await getIp();
    const info = await getInfo(ip);
    // const {city, country, regionName}

    console.log(info);
    const container = document.querySelector('#list');
    container.insertAdjacentHTML('beforeend', `
        <li>Continent: ${info.continent}</li>
        <li>Country: ${info.country}</li>
        <li>Region: ${info.region}</li>
        <li>City: ${info.city}</li>
        <li>District: ${info.district}</li>
    `)

})
