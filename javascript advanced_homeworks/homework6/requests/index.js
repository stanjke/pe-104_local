import { GET_INFO_API, GET_IP_API } from "../constants/api.js";
import { sendRequest } from "../helpers/sendRequest.js";

export const getIp = () => sendRequest(GET_IP_API);
export const getInfo = (ip) => sendRequest(`${GET_INFO_API}${ip}?fields=continent,country,region,city,district`);
// export const getUserPosts = (id) => sendRequest(`${API}users/${id}/posts`);
// export const removePost = (id) => getResponse(`${API}posts/${id}`, "DELETE");
