export async function sendRequest(url, method = "GET", options) {
  const response = await fetch(url, {
    method: method,
    ...options,
  });
  return response.json();
}

export async function getResponse(url, method = "GET", options) {
  const response = await fetch(url, {
    method: method,
    ...options,
  });
  return response;
}
