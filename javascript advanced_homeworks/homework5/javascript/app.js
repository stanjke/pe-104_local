import renderPosts from "./components/renderPosts.js";
import createNewPost from "./components/createPost.js";

renderPosts();
createNewPost();

{
    /* <div class="posts">
          <div class="card">
              <div class="card-buttons">
                  <button type="submit">Del</button>
                  <button type="submit">Edit</button>
              </div>
              <div class="user-info">
                  <div class="user-name">Name</div>
                  <div class="user-email">user@email.com</div>
              </div>
              <h3 class="title">Title head</h3>
              <p class="description">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odio, possimus?</p>
          </div>
      </div> */
}
