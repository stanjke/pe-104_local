import { API } from "../constants/api.js";
import { sendRequest, getResponse } from "../helpers/sendRequest.js";

export const getUsers = () => sendRequest(`${API}users`);
export const getPosts = () => sendRequest(`${API}posts`);
export const getUserPosts = (id) => sendRequest(`${API}users/${id}/posts`);
export const removePost = (id) => getResponse(`${API}posts/${id}`, "DELETE");
//implement editPost function with PATCH request:
export const patchPost = (title, body, id) => getResponse(`${API}posts/${id}`, "PATCH", {
    body: JSON.stringify({
        title: title,
        body: body,
    }),
    headers: {
        'Content-Type': 'application/json'
    }
})

export const setPost = (userId = 1, name, email, title, body, postId) => getResponse(`${API}posts`, "POST", {
    body: JSON.stringify({
        userId: userId,
        name: name,
        title: title,
        email: email,
        body: body,
        postId: postId
    }),
    headers: {
        'Content-Type': 'application/json'
    }
})

// fetch("http://ajax.test-danit.com/api/json/users/", {
//     method: 'POST',
//     body: JSON.stringify({
//       userId: 1,
//       name: 'John',
//       fullName: ['John', 'Marston']
//     }),
//     headers: {
//       'Content-Type': 'application/json'
//     }
//   })