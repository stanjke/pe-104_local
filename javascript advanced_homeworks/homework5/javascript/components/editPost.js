import EditModal from "./Modal/EditModal.js";
import { patchPost } from "../requests/index.js";

function editPost() {
    const editHandler = async (newTitle, newBody) => {
        const response = await patchPost(newTitle, newBody, this.postId);
        if (response.ok) {
            console.log("post was seccsussefully patched!");
            this.titleEl.innerText = newTitle;
            this.bodyEl.innerText = newBody;
        } else {
            console.log("something went wrong");
        }

    };

    new EditModal(this.title, this.body, editHandler).render()
}

export default editPost;