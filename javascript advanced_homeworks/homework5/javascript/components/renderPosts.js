import { getUsers, getPosts, getUserPosts } from "../requests/index.js";
import Card from "./Card/Card.js";
import deletePost from "./deletePost.js";
import editPost from "./editPost.js";

async function renderPosts() {
  const users = await getUsers();
  const container = document.querySelector(".content");
  const loader = document.querySelector(".loader");

  const usersPromisses = users.map(async ({ id, name, email }) => {
    const userPosts = await getUserPosts(id);
    userPosts.forEach(({ title, body, id: postId }) => {
      const post = new Card(name, email, title, body, postId, deletePost, editPost);
      post.render(container);
    });
  });
  loader.remove();
  await Promise.all(usersPromisses);
}

export default renderPosts;
