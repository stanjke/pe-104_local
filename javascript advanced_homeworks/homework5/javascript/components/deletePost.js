import { removePost } from "../requests/index.js";
import DeleteModal from "./Modal/DeleteModal.js";

function deletePost() {
  const removeHandler = async () => {
    const response = await removePost(this.postId);
    if (response.ok) {
      this.card.remove();
    }
  };
  new DeleteModal(this.title, removeHandler).render();
}

export default deletePost;
