class Card {
  constructor(name, email, title, body, postId, deleteFn, editFn) {
    this.card = document.createElement("div");
    this.cardBtns = document.createElement("div");
    this.delBtn = document.createElement("button");
    this.editBtn = document.createElement("button");
    this.infoWrap = document.createElement("div");
    this.userName = document.createElement("div");
    this.userEmail = document.createElement("div");
    this.titleEl = document.createElement("h3");
    this.bodyEl = document.createElement("p");
    this.deleteAction = deleteFn;
    this.editAction = editFn;
    this.name = name;
    this.email = email;
    this.title = title;
    this.body = body;
    this.postId = postId;
  }

  createEl() {
    this.card.classList.add("card");
    this.card.setAttribute("data-postId", `${this.postId}`);
    this.cardBtns.classList.add("card-buttons");
    this.delBtn.setAttribute("id", "delete");
    this.delBtn.classList.add("card__btn", "card__delete");
    this.editBtn.setAttribute("id", "edit");
    this.editBtn.classList.add("card__btn", "card__edit");
    this.infoWrap.classList.add("user-info");
    this.userName.classList.add("user-name");
    this.userEmail.classList.add("user-email");
    this.titleEl.classList.add("title");
    this.bodyEl.classList.add("description");
    this.userName.textContent = this.name;
    this.userEmail.textContent = this.email;
    this.titleEl.textContent = this.title;
    this.bodyEl.textContent = this.body;
    this.delBtn.addEventListener("click", this.deleteAction.bind(this));
    this.editBtn.addEventListener("click", this.editAction.bind(this));

    this.cardBtns.append(this.editBtn, this.delBtn);
    this.infoWrap.append(this.userName, this.userEmail);
    this.card.append(this.cardBtns, this.infoWrap, this.titleEl, this.bodyEl);
  }

  render(parent = document.body) {
    this.createEl();
    parent.append(this.card);
  }

  renderBefore(parent) {
    this.createEl();
    parent.prepend(this.card);
  }
}

export default Card;

{
  /* <div class="card">
        <div class="card-buttons">
            <button type="submit">Del</button>
            <button type="submit">Edit</button>
        </div>
        <div class="user-info">
            <div class="user-name">${name}</div>
            <div class="user-email">${email}</div>
        </div>
        <h3 class="title">${title}</h3>
        <p class="description">${body}</p>
</div> */
}
