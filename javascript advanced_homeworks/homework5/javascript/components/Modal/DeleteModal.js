import Modal from "./Modal.js";

class DeleteModal extends Modal {
  constructor(title, submitFn) {
    super();
    this.title = title;
    this.submitFn = submitFn;
    this.btnConfirm = document.createElement("button");
    this.btnCancel = document.createElement("button");
    this.submitTitle = document.createElement("h3");
  }

  createEl() {
    super.createEl();
    this.btnConfirm.textContent = "Confirm";
    this.btnCancel.textContent = "Cancel";
    this.btnConfirm.classList.add("modal__confirm-btn");
    this.btnCancel.classList.add("modal__cancel-btn");
    this.submitTitle.textContent = `Do you realy want to delete "${this.title}" ?`;
    this.divModalContWrap.append(this.submitTitle);
    this.divModalBtnWrap.append(this.btnConfirm, this.btnCancel);
    this.btnCancel.addEventListener("click", this.closeModal.bind(this));
    this.btnConfirm.addEventListener("click", () => {
      this.submitFn();
      this.closeModal();
    });
  }
}

export default DeleteModal;
