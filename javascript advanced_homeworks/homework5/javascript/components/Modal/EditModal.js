import Modal from "./Modal.js";

class EditModal extends Modal {
  constructor(input, body, submitFn) {
    super();
    this.input = input;
    this.body = body;
    this.submitFn = submitFn
    this.btnConfirm = document.createElement('button');
    this.btnCancel = document.createElement('button');
    this.editInput = document.createElement('input');
    this.editBody = document.createElement('textarea');
  }

  createEl() {
    super.createEl();
    this.btnConfirm.textContent = "Confirm";
    this.btnCancel.textContent = "Cancel";
    this.btnConfirm.classList.add("modal__confirm-btn");
    this.btnCancel.classList.add("modal__cancel-btn");
    this.editInput.value = this.input;
    this.editBody.value = this.body;
    this.divModalContWrap.append(this.editInput, this.editBody)
    this.divModalBtnWrap.append(this.btnConfirm, this.btnCancel)
    this.btnCancel.addEventListener('click', this.closeModal.bind(this))
    this.btnConfirm.addEventListener('click', () => {
      this.submitFn(this.editInput.value, this.editBody.value);
      this.closeModal();
    })
  }
}

export default EditModal;