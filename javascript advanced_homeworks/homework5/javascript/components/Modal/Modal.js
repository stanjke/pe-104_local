class Modal {
  constructor() {
    this.divModal = document.createElement("div");
    this.divModalBg = document.createElement("div");
    this.divMainCont = document.createElement("div");
    this.btnModalClose = document.createElement("button");
    this.divModalContWrap = document.createElement("div");
    this.divModalBtnWrap = document.createElement("div");
  }

  createEl() {
    this.divModal.classList.add("modal");
    this.divModalBg.classList.add("modal__background");
    this.divMainCont.classList.add("modal__main-container");
    this.btnModalClose.classList.add("modal__close");
    this.divModalContWrap.classList.add("modal__content-wrapper");
    this.divModalBtnWrap.classList.add("modal__button-wrapper");
    this.divMainCont.append(this.btnModalClose, this.divModalContWrap, this.divModalBtnWrap);
    this.divModal.append(this.divModalBg, this.divMainCont);
    this.btnModalClose.addEventListener("click", this.closeModal.bind(this));
    this.divModalBg.addEventListener("click", this.closeModal.bind(this));
  }

  closeModal() {
    this.divModal.remove();
  }

  render(container = document.body) {
    this.createEl();
    container.append(this.divModal);
  }
}

export default Modal;
