import Modal from "./Modal.js";

class CreateModal extends Modal {
    constructor(submitFn) {
        super();
        this.submitFn = submitFn
        this.btnConfirm = document.createElement('button');
        this.btnCancel = document.createElement('button');
        this.createInput = document.createElement('input');
        this.createBody = document.createElement('textarea');
    }
    createEl() {
        super.createEl();
        this.btnConfirm.textContent = "Confirm";
        this.btnCancel.textContent = "Cancel";
        this.btnConfirm.classList.add("modal__confirm-btn");
        this.btnCancel.classList.add("modal__cancel-btn");
        this.divModalContWrap.append(this.createInput, this.createBody)
        this.divModalBtnWrap.append(this.btnConfirm, this.btnCancel)
        this.btnCancel.addEventListener('click', this.closeModal.bind(this))
        this.btnConfirm.addEventListener('click', () => {
            this.submitFn(this.createInput.value, this.createBody.value);
            this.closeModal();
        })
    }
}

export default CreateModal;