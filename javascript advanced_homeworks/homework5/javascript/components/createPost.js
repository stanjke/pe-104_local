import CreateModal from "./Modal/CreateModal.js";
import Card from "./Card/Card.js";
import { setPost } from "../requests/index.js";
import deletePost from "./deletePost.js";
import editPost from "./editPost.js";

function createNewPost() {
    const container = document.querySelector(".content");
    const createBtn = document.querySelector('#createBtn');
    createBtn.addEventListener('click', () => {
        const randomPostId = Math.floor(Math.random() * 10);
        new CreateModal(submitHandler).render();

        async function submitHandler(newInput, newBody, name = "John", email = "test@mail.com", postId = randomPostId) {
            this.createInput.innerText = newInput;
            this.createBody.innerText = newBody;
            const response = await setPost(1, name, email, this.createInput.value, this.createBody.value, postId);
            if (response.ok) {
                const newPost = new Card(name, email, this.createInput.value, this.createBody.value, postId, deletePost, editPost);
                newPost.renderBefore(container);
            }
        }
    })
}

export default createNewPost;