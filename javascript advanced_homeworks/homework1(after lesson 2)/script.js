// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// В жс все объекты имеют свойство [prototype], оно может ссылаться на другой объект, либо
// равняться null. Если мы хотим унаследовать методы и свойства другого объекта, то мы можем
// воспользоваться специальной конструкцией __proto__ (ее еще называют историческим геттером/сеттером),
// например: 
// const target = {
//   foo() {
//     console.log("hello world");
//   }
// };
// const current = {};
// current.__proto__ = target
// current.foo(); // "hello world"

// Этот код работает следующим образом, на самом деле как такового метода у "foo" в объекте "current" нету,
// жс сперва проходится по самому объекту "current", не найдя этот метод в нем, он идет в prototype,
// там он находит "foo", который мы унаследовали с помощью __proto__ и вызывает его от туда.






// Для чого потрібно викликати super() у конструкторі класу-нащадка?

// super() используют в конструкторе потомка для того что бы унаследовать 
// свойства и методы родительского конструктора.


class Employee {
  constructor(name = "John", age = 25, salary = 5000) {
    this._name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(val) {
    this._name = val;
  }

  get name() {
    return this._name;
  }

  set age(val) {
    this._age = val;
  }

  get age() {
    return this._age;
  }

  set salary(val) {
    this._salary = val;
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(lang = JS, ...args) {
    super(...args);
    this.lang = lang;
  }

  set salary(val) {
    this._salary = val * 3;
  }

  get salary() {
    return this._salary;
  }
}

const worker = new Employee("Garry", 23, 10000);
console.log(worker);
const js1 = new Programmer("python");
console.log(js1);
const js2 = new Programmer("C#", "Tom", 30, 10000);
console.log(js2);
const js3 = new Programmer("Ruby", "Ben", 35, 12000);
console.log(js3);
