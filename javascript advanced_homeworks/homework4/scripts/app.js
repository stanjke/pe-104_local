// AJAX  это технология в JavaScript, которая позволяет получать данные с сервера без 
// перезагрузки страницы. Для этого обычно используют объекты :
// - XMLHttpRequest;
// - и более новый  Fetch API;

const url = "https://ajax.test-danit.com/api/swapi/films";

async function sendRequest(url, method = 'GET', options) {
  const response = await fetch(url, {
    method: method,
    ...options
  })
  return response.json();
}

async function showFilmDetails() {
  const container = document.querySelector(".container");
  const films = await sendRequest(url);
  for (const {characters, episodeId, name, openingCrawl} of films) {
    const card = new Card(episodeId, name, openingCrawl);
    card.render(container);

    const characterList = await Promise.all(
        characters.map(async (character) => {
          const characterInfo = await sendRequest(character);
          const {name} = characterInfo;
          return name;
        })
    )

    card.loadCharacters(characterList);
  }
  // films.forEach(async ({ characters, episodeId, name, openingCrawl }) => {
  //   const card = new Card(episodeId, name, openingCrawl);
  //   card.render(container);

  //   const charactersList = await Promise.all(
  //     characters.map(async (character) => {
  //       const characterInfo = await sendRequest(character)
  //       const { name } = characterInfo;
  //       return name;
  //     })
  //   );
  //   card.loadCharacters(charactersList);
  // });
}

class Card {
  constructor(episode, name, description) {
    this.div = document.createElement("div");
    this.filmName = document.createElement("h2");
    this.filmEpisode = document.createElement("h4");
    this.filmDescription = document.createElement("p");
    this.characterWrap = document.createElement("div");
    this.characterTitle = document.createElement("h4");
    this.characterList = document.createElement("ul");
    this.episode = episode;
    this.name = name;
    this.description = description;
  }

  createCard() {
    this.div.classList.add("card");
    this.filmName.textContent = this.name;
    this.filmEpisode.classList.add("film__episode");
    this.filmEpisode.textContent = this.episode;
    this.filmDescription.classList.add("film__description");
    this.filmDescription.textContent = this.description;
    this.characterWrap.classList.add("character__wrap");
    this.characterTitle.classList.add("character__title");
    this.characterTitle.textContent = "Characters:";
    this.characterList.classList.add("character__list", 'loader');

    this.characterWrap.append(this.characterTitle, this.characterList);
    this.div.append(this.filmName, this.filmEpisode, this.filmDescription, this.characterWrap);
  }

  loadCharacters(characters) {
    this.characterList.classList.remove('loader')
    characters.forEach((character) => {
          const item = document.createElement("li");
          item.textContent = character;
          this.characterList.append(item);
        });
    }

  render(parent = document.body) {
    this.createCard();
    parent.append(this.div);
  }
}

showFilmDetails();
