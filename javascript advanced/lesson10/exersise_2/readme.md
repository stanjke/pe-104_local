# Users

Використовуючи `async/await` отримати по вказаному URL об'єкти користувачів кількість на вибір

```js

const url = 'https://randomuser.me/api';

```

<blockquote>
    Сервер повертає помилки на більшість запитів. Намагайтесь отримати 5-6 позитивних відповідей.
</blockquote>

Відмалювати в `<div class="users_list"></div>` карточки типу:

```html

<div class="user card user_card">
    <div class="details">
        <div class="user-photo">
            <img src="${this.picture}" alt="${this.name}">	
        </div>
        <p class="user-label user_label">Hi, My name is</p>
        <p class="user-value user_value">${this.fullName}</p>				

        <ul class="values-list values_list">
            <li class="list-item" data-title="Hi, My name is" data-value="${this.fullName}">Name</li>
            <li class="list-item" data-title="My gender is" data-value="${this.gender}">Gender</li>
            <li class="list-item" data-title="My address is" data-value="${this.city}">City</li>
            <li class="list-item" data-title="My phone number is" data-value="${this.phone}">Tel</li>
            <li class="list-item" data-title="My email is" data-value="${this.email}">Email</li>
        </ul>
    </div>
</div>

```

За допомогою класса UserCard та методом render



Зробити функцію valuesList яка буде працювати з списком values_list при наведенні на list-item повинен додаватись класс active та передаватись данні з атрибутів data-title  data-value в теги user_label та user_value





