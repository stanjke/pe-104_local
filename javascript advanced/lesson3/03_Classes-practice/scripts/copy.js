{
  /* <div class="card">
    <button class="card__btn card__edit"></button>
    <button class="card__btn card__delete"></button>
    <div class="card__content-container"></div>
</div> */
}

function deleteContent() {
  console.log("delete action", this);
  // this.remove();
  const submitHandler = () => {
    this.divCard.remove();
  };
  new DeleteModal(this.title, submitHandler).render();
}

function editContent() {
  console.log("edit action", this);
}

class Card {
  constructor(deleteF, editF) {
    this.divCard = document.createElement("div");
    this.btnEdit = document.createElement("button");
    this.btnDel = document.createElement("button");
    this.divContent = document.createElement("div");
    this.deleteAction = deleteF;
    this.editAction = editF;
  }

  createEl() {
    this.divCard.classList.add("card");
    this.btnEdit.classList.add("card__btn", "card__edit");
    this.btnDel.classList.add("card__btn", "card__delete");
    this.divContent.classList.add("card__content-container");
    this.divCard.append(this.btnEdit, this.btnDel, this.divContent);
    this.btnDel.addEventListener("click", this.deleteAction.bind(this));
    this.btnEdit.addEventListener("click", this.editAction.bind(this));
  }

  render(container = document.body) {
    this.createEl();
    container.append(this.divCard);
  }
}

class ArticleCard extends Card {
  constructor(title, text, deleteF, editF) {
    super(deleteF, editF);
    this.title = title;
    this.text = text;
    this.titleContainer = document.createElement("h3");
    this.textContainer = document.createElement("p");
  }

  createEl() {
    super.createEl();
    this.titleContainer.innerText = this.title;
    this.textContainer.innerText = this.text;
    this.divContent.append(this.titleContainer, this.textContainer);
  }
}

{
  /* <div class="modal">
    <div class="modal__background"></div>

    <div class="modal__main-container">
        <button class="modal__close"></button>
        <div class="modal__content-wrapper"></div>
        <div class="modal__button-wrapper"></div>
    </div>
</div> */
}

class Modal {
  constructor() {
    this.divModal = document.createElement("div");
    this.divModalBg = document.createElement("div");
    this.divModalContainer = document.createElement("div");
    this.modalBtnClose = document.createElement("button");
    this.modalContentWrap = document.createElement("div");
    this.modalBtnWrap = document.createElement("div");
  }

  createEl() {
    this.divModal.classList.add("modal");
    this.divModalBg.classList.add("modal__background");
    this.divModalContainer.classList.add("modal__main-container");
    this.modalBtnClose.classList.add("modal__close");
    this.modalContentWrap.classList.add("modal__content-wrapper");
    this.modalBtnWrap.classList.add("modal__button-wrapper");
    this.divModalContainer.append(this.modalBtnClose, this.modalContentWrap, this.modalBtnWrap);
    this.divModal.append(this.divModalBg, this.divModalContainer);
    this.modalBtnClose.addEventListener("click", this.closeModal.bind(this));
    this.divModalBg.addEventListener("click", this.closeModal.bind(this));
  }

  render(container = document.body) {
    this.createEl();
    container.append(this.divModal);
  }

  closeModal() {
    this.divModal.remove();
  }
}

class DeleteModal extends Modal {
  constructor(title, submitAction) {
    super();
    this.title = title;
    this.submitAction = submitAction;
    this.submitTitle = document.createElement("h3");
    this.submitBtn = document.createElement("button");
    this.cancelBtn = document.createElement("button");
  }

  createEl() {
    super.createEl();
    this.submitBtn.innerText = "Confirm";
    this.submitBtn.classList.add("modal__confirm-btn");
    this.cancelBtn.innerText = "Cancel";
    this.cancelBtn.classList.add("modal__cancel-btn");
    this.submitTitle.innerText = `Do you realy want to delete ${this.title}`;

    // this.modalContentWrap.append(this.);
    this.modalBtnWrap.append(this.submitBtn, this.cancelBtn);
    this.cancelBtn.addEventListener("click", this.closeModal.bind(this));
    this.submitBtn.addEventListener("click", () => {
      this.submitAction();
      this.closeModal();
    });
  }
}

const textCard = new Card(deleteContent, editContent);
// console.log(textCard);
const container = document.querySelector(".container");
textCard.render(container);
// const testArticle = new ArticleCard(posts[0].title, posts[0].body, deleteContent, editContent);
posts.forEach((item) => {
  new ArticleCard(item.title, item.body, deleteContent, editContent).render();
});

const mod = new Modal().render();
