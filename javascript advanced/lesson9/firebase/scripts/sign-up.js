const url = "https://dan-it-lesson9-default-rtdb.europe-west1.firebasedatabase.app/";
const form = document.querySelector(".form");

function submitHandler(event) {
  event.preventDefault();
  const name = document.querySelector("#name").value;
  const age = document.querySelector("#age").value;
  const city = document.querySelector("#city").value;
  const email = document.querySelector("#email").value;
  const password = document.querySelector("#password").value;
  const passwordRepeat = document.querySelector("#repeatPassword").value;

  if (password !== passwordRepeat) {
    return alert("passwor not passed");
  }

  const user = {
    name,
    age,
    city,
    email,
    password,
    id: `id${Date.now()}`,
    avatar: "https://i.pravatar.cc/300",
    todoList: [
      {
        title: "Onboarding",
        taskId: `taskId${Date.now()}`,
      },
    ],
  };

  fetch(`${url}/users.json`, {
    method: "POST",
    body: JSON.stringify(user),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log("User created", data);
    })
    .catch((error) => console.log("Error creating user", error));

  form.reset();

  alert("User was seccsussfuly created");

  window.location.href = "../sign-in";

  console.log(event);
}

form.addEventListener("submit", submitHandler);
