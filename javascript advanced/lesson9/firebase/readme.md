# Регистрация, авторизация и список дел :)
<hr />

# Регистрация

На странице `sign-up` есть форма регистрации.

1. Собрать все данные с формы и отправить их в `body` c помощью `POST` запроса на свою базу даних (смотри readme api).
2. В случае ответа от сервера об успешной регистрации вывести сообщение для пользователя и сделать редирект на страницу авторизации `sign-in`.
3. Обработать возможные ошибки.


# Авторизация
На странице `sign-in` есть форма авторизации.

1. Собрать все данные с формы и отправить их в `body` c помощью `POST` запроса на на свою базу даних 
2. В случае ответа от сервера об успешной авторизации:
   1. Сохранить в `sessionStorage` или `localStorage` данные пользователя и токен авторизации.
   2. сделать редирект на главную страницу пользователя `/`.
3. Обработать возможные ошибки (смотри readme api).

# Главная страница (список дел)

1. Проверять есть ли в хранилище данные пользователя, и если есть - отрисовать в `<div class="user-container"></div>` html
    ```html
        <img src="${avatar}" alt="${name}">
        <p>${name}</p>
        <span>Age: ${age}</span>
        <span>City: ${city}</span>
        <form class="new-todo">
            <input type="text" name="todo" id="todo" placeholder="Add todo">
            <button type="submit">Add</button>
        </form>
    ```

2. Проверять есть ли у пользователя записи в списке дел (`user.toDoList`). Если есть - отрисовать в `<div class="todo-container"></div>`
   toDoItem используя класс ToDoItem.

3. Навесить на форму `<form class="new-todo">` на событие `submit` отправку `POST` запроса на свою базу даних
   1. В случае успешного ответа - обновлять в `sessionStorage` или `localStorage` данные пользователя и перерисовывать список дел.
   2. Обработать возможные ошибки (смотри readme api).

4. Навесить на `Delete` кнопку на событие `click` отправку `DELETE` запроса на свою базу даних
   1. В случае успешного ответа - обновлять в `sessionStorage` или `localStorage` данные пользователя и перерисовывать список дел.
   2. Обработать возможные ошибки (смотри readme api).

5. Дописать функционал кнопки `Log out` в хедере. При клике удалять данные из хранилища и перезагружать страницу.
