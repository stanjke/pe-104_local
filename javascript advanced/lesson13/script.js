import { API } from "./api.js";




const container = document.querySelector('#root');

class Person {
    constructor(name, height, mass, hairColor, gender, films) {
        this.name = name;
        this.height = height;
        this.mass = mass;
        this.hairColor = hairColor;
        this.gender = gender;
        this.films = films;
    }

    render(parent) {
        parent.insertAdjacentHTML('beforeend', `
        <li>
        <h3>${this.name}</h3>
        <p>Height: ${this.height}</p>
        <p>Mass: ${this.mass}</p>
        <p>Hair color: ${this.hairColor}</p>
        <p>Gender : ${this.gender}</p>
        <p>Film: ${this.film}</p>
    </li>
        `)
    }

}

class PersonList {
    constructor() {
        this.persons = [];
        this.sortOptions = [];
        this.sortKey = '';
        this.list = document.createElement('ul');
    }

    async fetchPersons () {
        const response = await fetch(API);
        const data = await response.json();
        this.persons = data.map(({
            name, 
            height, 
            mass, 
            hairColor, 
            gender, 
            films}) => new Person(name, height, mass, hairColor, gender, films));
        console.log(data);
        this.sortOptions = Object.keys(this.persons[0]);
        console.log(this.sortOptions);
        // console.log(Object.keys(this.persons[0]));
        // this.sortOptions = 
        this.createSortElements();
        this.renderPersons();
    }

    createSortElements() {
        const sortContainer = document.createElement('div');
        const sortLabel = document.createElement('label');
        const sortDropDown = document.createElement('select');
        sortLabel.textContent = 'Sort by:';
        sortDropDown.id = 'sort';
        sortLabel.setAttribute('for', 'sort');
        console.log(this.sortOptions);
        this.sortOptions.forEach( option => {
            const optionElement = document.createElement('option');
            optionElement.value = option;
            optionElement.textContent = option;
            sortDropDown.append(optionElement);
        })
        sortContainer.append(sortLabel, sortDropDown);
        container.append(sortContainer);

        sortDropDown.addEventListener('change', (e) => {
            this.sortKey = e.target.value;
            console.log(this.sortKey);
            this.applySort();
        })
    }

    applySort() {
        this.persons.sort((a, b) => {
            return a[this.sortKey] - b[this.sortKey];
        });
        console.log(this.persons);
        this.list.innerHTML = '';
        if (this.list) {
            this.renderPersons();
        }


    }

    renderPersons() {
        this.list.insertAdjacentHTML('beforeend', `

        ${this.persons.forEach(item => item.render(this.list))}
    `)
        container.append(this.list)
    }
}

const test = new PersonList().fetchPersons();
// const person = new Person().render();