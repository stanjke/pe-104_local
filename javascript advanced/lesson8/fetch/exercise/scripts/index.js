const url = "https://ajax.test-danit.com/api/json/photos";
const containers = document.querySelectorAll(".container");

fetch(url)
  .then((response) => {
    const { status, ok } = response;
    if (ok) {
      return response.json();
    } else {
      throw new Error(`Bad status: ${status}`);
    }
  })
  .then((data) => {
    data.map(({ albumId, title, url }) => {
      try {
        if (albumId <= containers.length) {
          const card = new Card(title, url);
          const container = document.querySelector(`#container_${albumId}`);
          card.render(container);
        }
      } catch (error) {}
    });
  });

class Card {
  constructor(title, url) {
    this.title = title;
    this.url = url;
    this.div = document.createElement("div");
    this.img = document.createElement("img");
    this.span = document.createElement("span");
  }

  createEl() {
    this.img.src = this.url;
    this.span.textContent = this.title;
    this.div.classList.add("card");
    this.div.append(this.img, this.span);
  }

  render(parent) {
    this.createEl();
    parent.append(this.div);
  }
}
