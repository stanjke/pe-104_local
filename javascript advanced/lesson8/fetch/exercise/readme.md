Отримати список зображень по `https://ajax.test-danit.com/api/json/photos` и відмалювати їх в контейнер id котрого дорівнює id альбому зображення.
Формат карточки:

```html

<div class="card">
    <img src="" alt="">
    <span>Title</span>
</div>

```
