# Пошук університетів?

По кліку на кнопку зчитувати країну яка вибрана в селект і відправити `GET` запит 
на `http://universities.hipolabs.com/search?country=${country}`.

Рендерити в `ul` посилання на сайт кожного університету.

Під час завантаження показувати прелоадер ( можна просто слово Loading... )
