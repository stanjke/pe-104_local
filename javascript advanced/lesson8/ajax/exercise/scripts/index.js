const url = "http://universities.hipolabs.com/search?country=";
const btn = document.querySelector("button");
const list = document.querySelector("ul");
const select = document.querySelector("#country");
const xhr = new XMLHttpRequest();

function sendAjaxRequest(url) {
  return new Promise((resolve, reject) => {
    const getCountry = select.value;
    xhr.open("GET", `${url}${getCountry}`);
    xhr.onload = () => {
      if (xhr.status === 200) {
        const dataArr = JSON.parse(xhr.response);
        resolve(dataArr);
      } else {
        reject(console.error(`Bad request! Status: ${xhr.status}`));
      }
    };
    xhr.send();
  });
}

btn.addEventListener("click", () => {
  const loadingMsg = document.createElement("p");
  loadingMsg.innerText = "Loading data...";
  list.append(loadingMsg);

  sendAjaxRequest(url)
    .then((universeList) => {
      loadingMsg.remove();
      universeList.map(({ name, web_pages: webPages }) => {
        const item = document.createElement("li");
        const universeName = document.createElement("p");
        const link = document.createElement("a");
        universeName.innerText = `${name}`;
        link.innerText = `${webPages[0]}`;
        link.href = webPages[0];
        link.target = "_blank";
        item.append(universeName, link);
        list.append(item);
      });
    })
    .catch((error) => {
      console.log("something went wrong...");
    });
});

// const getCountry = select.value;
// xhr.open("GET", `${url}${getCountry}`);
// xhr.send();
// xhr.onload = () => {
//   if (xhr.status === 200) {
//     const dataArr = JSON.parse(xhr.response);
//     console.log(dataArr);
//   } else {
//     console.error(`Bad request! Status: ${xhr.status}`);
//   }
// };
