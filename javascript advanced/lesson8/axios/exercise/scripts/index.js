// https://ajax.test-danit.com/api/swapi/vehicles
const api = "https://ajax.test-danit.com/api/swapi/vehicles";
const section = document.querySelector(".container");

axios
  .get(api)
  .then(({ data }) => {
    console.log(data);

    data.map(async ({ name, model, films }) => {
      const filmDataPromises = films.map((film) => {
        return axios
          .get(film)
          .then(({ data }) => {
            const { episodeId, name: filmName } = data;
            return { episodeId, filmName };
          })
          .catch((error) => console.error(error));
      });

      const filmData = await Promise.all(filmDataPromises);

      const card = new Card(name, model, filmData);
      card.render(section);
    });
  })
  .catch((error) => console.error(error));

class Card {
  constructor(name, model, filmName) {
    this.div = document.createElement("div");
    this.pName = document.createElement("p");
    this.pModel = document.createElement("p");
    this.ul = document.createElement("ul");
    this.name = name;
    this.model = model;
    this.filmName = filmName;
  }

  createEl() {
    this.div.classList.add("card");
    this.pName.textContent = `Name : ${this.name}`;
    this.pModel.textContent = `Model : ${this.model}`;
    this.div.append(this.pName, this.pModel);

    this.filmName.forEach(({ episodeId, filmName }) => {
      const li = document.createElement("li");
      li.textContent = `Episode ${episodeId} - ${filmName}`;
      this.ul.append(li);
    });
    this.div.append(this.ul);
  }

  render(parent) {
    this.createEl();
    parent.append(this.div);
  }
}
