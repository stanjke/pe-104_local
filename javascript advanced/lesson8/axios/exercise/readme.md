Використати axios

Отримати список машин за адресою https://ajax.test-danit.com/api/swapi/vehicles, відмалювати в <section class="container"> для кожної машини:

```html

<div class="card">
    <p>Name: ${name}</p>
    <p>Model: ${model}</p>
    <ul></ul>
</div>

```

Кожний об'єкт машини утримує в собі масив з посиланнями на іеформацію про фільми в котрих ці машини були. Відмалювати назви фільмів у вигляді  `<li>Episode ${num} - ${name}</li>` в `<ul>` для кожної карточки.  
