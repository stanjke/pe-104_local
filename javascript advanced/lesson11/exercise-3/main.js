/**
Задание:
	1. напишите сокращеные функции getById (возвращает DOM-элемент по id),
getByClass (возвращает массив DOM-Элементов по классу),
queryAll (возвращает массив DOM-Элементов по селектору).
2. Каждую функцию поместите в  отдельный модуль.
3. Импортируйте их в один файл.
4. Импортируйте getByClass внутри main.js

 */

import {getByClass, getById, queryAll} from './js/functions/index.js';

console.log(getByClass('list-item'));
console.log(getById('test'));
console.log(queryAll('[class="list-item"]'));