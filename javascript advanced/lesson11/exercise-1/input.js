import INPUT_TYPES from "./types.js";

export default class Input {
	constructor({type, className, id}) {
        if (!INPUT_TYPES.includes(type)) {
            throw new Error("Incorrect type");
        }
		this.type = type;
		this.className = className;
		this.id = id;
	}

	render() {
		const input = document.createElement('input');
		input.type = this.type;
		input.className = this.className;
		input.id = this.id;
		return input;
	}
 }