/**
 * Задание:
	1. Напишите класс "Input", который создает объект, описывающий однострочное
поле ввода.
	У объекта будут такие свойства:
	- тип поля ввода;
- классы;
- id;

и метод:
	- render, возвращающий DOM-элемент, созданный на основе свойств объекта.

	Вынесите этот класс в отдельный модуль, импортируйте ее в основной файл main.js,
	создайте однострочное поле ввода и выведите его на экран.

 <input type="text" class="form-control size-lg" id="user-name">

 */

import InputSearh from './input.js'

const search = {
	type: 'password',
	id: 'search',
	className: 'input-box input-search',
}

const searchInput = new InputSearh(search);

document.body.append(
	searchInput.render()
	);