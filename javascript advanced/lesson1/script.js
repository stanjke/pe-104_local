const manager = function (name, sales) {
  return {
    name: name,
    sales: sales,
    sell: function (thing) {
      this.sales += 1;
      return "Manager" + this.name + " sold " + thing;
    },
  };
};

const stas = manager("Stas", 10);
stas.sell("pineapple");
console.log(stas.sales);
