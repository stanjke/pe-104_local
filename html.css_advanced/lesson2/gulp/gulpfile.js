
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const files = {
    scssPath: './src/scss/**/*.scss',
    jsPath: './src/js/**/*.js'
}

function buildStyles() {
    return gulp.src(files.scssPath)
                .pipe(sass().on('error', sass.logError))
                .pipe(gulp.dest('./assets/css'))
}

exports.buildStyles = buildStyles;

function defaultTask(cb) {
    // place code for your default task here
    cb();
  }
  
  exports.default = defaultTask