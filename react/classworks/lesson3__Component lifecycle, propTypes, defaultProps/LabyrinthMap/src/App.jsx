import './App.css'

function App() {


  return (
      <div className="art-board">
          <div className="art-board-labyrinth">
              <div className="row">
                  <div className="wall-cell"></div>
                  <div className="space-cell"></div>
                  ....
              </div>
              ....
          </div>
          <div className="art-board-navigation">
              <div className="row-map-size item">
                  <div className="input-group">
                      <span className="input-group-placeholder">Size</span>
                      <input type="number" id="mapSize"/>
                  </div>
                  <button type="button" className="button">Map size</button>
              </div>
              <div className="button-wrapper item">
                  <button type="button" className="button">Refresh Map</button>
              </div>
          </div>
      </div>
  )
}

export default App
