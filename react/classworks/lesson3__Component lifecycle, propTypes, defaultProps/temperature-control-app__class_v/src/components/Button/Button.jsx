import React, {useState} from "react";
import cn from 'classnames';
import PropTypes from 'prop-types'
import './Button.scss'
export default function Button (props){
		const {
			classNames, 
			type, 
			click, 
			children,
			...restProps
		} = props;
		return(
			<button className={cn('button', classNames)} type={type} onClick={click} {...restProps}>{children}</button>
		)

}

Button.defaultProps = {
	type: "button",
	click: () => {},
}
 Button.propTypes = {
	type: PropTypes.string,
	classNames: PropTypes.string,
	click: PropTypes.func,
	children: PropTypes.any,
 }
