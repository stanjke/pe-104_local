import React from "react";
import Meta from './components/Meta'
import Forecast from './components/Forecast'

import {API_URL, API_KEY} from "../../configs/API";
import {sendRequest} from "../../helpers/sendRequest";
import { useState } from "react";
import { useEffect } from "react";

const city='kiev';
const days=10;
export default function Weather(){

    const [current, setCurrent] = useState({});
    const [forecast, setForecast] = useState([]);
    const [locale, setLocale] = useState("");


    useEffect(() => {
        sendRequest(`${API_URL}?key=${API_KEY}&q=${city}&days=${days}&aqi=no&alerts=no`)
            .then(({current, forecast, location}) =>{

                const daysArr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',];
                const monthsArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

                const newForecast=forecast.forecastday.map(item=>{
                    const newDate = new Date(item.date);
                    const weekDay = newDate.getDay(); // 0-6
                    const day = newDate.getDate();
                    const month = newDate.getMonth(); // 0-11
                    return {
                        forecastIcon:item.day.condition.icon,
                        weekDay:daysArr[weekDay],
                        month:monthsArr[month],
                        day:day,
                        forecastText:item.day.condition.text,
                        maxTemp:item.day.maxtemp_c,
                    }
                });
                setCurrent({
                    cloud:current.cloud,
                    humidity:current.humidity,
                    temp:current.temp_c})
                setForecast(newForecast)
                setLocale(location.name)
            });
    })


        return(
            <div className="weather-wrapper">
                <Meta
                    feelslike = {current.temp}
                    cloud = {current.cloud}
                    humidity = {current.humidity}
                />
                <Forecast
                   data = {forecast}
                />
            </div>
        )
}