import React from "react";
import ForecastCard from "./ForecastCard.jsx";
import './Forecast.scss'

export default function Forecast({data}) {
		const forecastCards = data.map((card, index) => <ForecastCard
			card={card}
			key={index}
		/>)

		if(!data) {
			return (
				<div>Ops...</div>
			)
		}

		return (
			<div className="forecast">
				{forecastCards}
			</div>
		)
}
