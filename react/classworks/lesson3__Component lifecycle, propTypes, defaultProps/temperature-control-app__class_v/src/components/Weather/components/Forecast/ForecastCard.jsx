import React from "react";
import cn from "classnames";
import PropTypes from "prop-types";

export default function ForecastCard({card}) {
        const {weekDay, day, month, forecastIcon,forecastText,maxTemp} = card;
        const currentDate = new Date().getDate();


        return(
            <div className= {cn('forecast__card', {'card--selected': currentDate === day})}>
                <p className="card__data">{weekDay} <br/> {day} {month}</p>
                <div className="card__icon">
                    <img src={forecastIcon} alt={forecastText} />
                </div>
                <span className="card__temp">{maxTemp}</span>
            </div>
        )

}

ForecastCard.propTypes = {
    card: PropTypes.shape({
        weekDay: PropTypes.number, 
        day: PropTypes.number, 
        month: PropTypes.string, 
        forecastIcon: PropTypes.string ,
        forecastText: PropTypes.string,
        maxTemp: PropTypes.number
    })
}