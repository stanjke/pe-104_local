import React from "react";
import cn from "classnames";
import PropTypes from "prop-types";


// class WrapperApp extends React.Component {

//     render() {
//         const {temperature, children} = this.props;
//         const backgoundColor = () => {
//             const currentClass = ["widget-container"]
//             if(temperature<10){
//                 currentClass.push("cold")
//             } else if(temperature>20){
//                 currentClass.push("hot")
//             } else {
//                 currentClass.push("neutral")
//             }
//             return currentClass.join(" ")
//         }

//         return (
//             <div className={backgoundColor()}>
//                 {children}
//             </div>
//         )
//     }
// }

// export default WrapperApp;

export default function WrapperApp({temperature, children}) {
            return (
            <div className={cn(
                'widget-container', 
                {hot: temperature > 20},
                {cold: temperature < 10},
                {neutral:temperature >= 10 || temperature <= 20}
                )}>
                {children}
            </div>
        )

}

WrapperApp.propTypes = {
	temperature: PropTypes.number,
    children: PropTypes.any,

 }