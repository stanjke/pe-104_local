import React from "react";
import Button from "../Button/index.js";
import PropTypes from "prop-types";

export default function Controller(props) {
        const {increase, decrease, showModal} = props;

        return(
            <div className="widget-controllers">
                <div className="button-container">
                    <Button click={decrease} >Cold</Button>
                    <Button click={increase} >Hot</Button>
                    <Button click={showModal}>Modal</Button>
                </div>
            </div>
        )
}

Controller.propTypes = {
    decrease: PropTypes.func,
    increase: PropTypes.func,
    showModal: PropTypes.func
    }