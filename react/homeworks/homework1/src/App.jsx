import { useState } from 'react'

import Modal from './components/Modal'
import AppWrap from './components/AppWrap'
import { FIRST_MODAL, SECOND_MODAL } from "./constants/nameConstants";

import './App.css'

function App() {

  const [isModal, setModal] = useState(false);
  const [modalType, setModalType] = useState("");
  const [modalContent, setModalContent] = useState({
    title: '',
    body: ''
  })

  const handleModal = () => {
    setModal(!isModal);
  }

  const handleModalType = (event) => {
    switch (event.target.textContent) {
      case FIRST_MODAL:
        return (
          setModalType("modal-first"),
          setModalContent({
            title: FIRST_MODAL,
            body: "this is first MODAL"
          })
        )
      default:
        return (
          setModalType("modal-second"),
          setModalContent({
            title: SECOND_MODAL,
            body: "this is second MODAL"
          })
        )
    }
  }

  const handleOutside = (event) => {
    if(!event.target.closest(".modal")) {
       handleModal();
    }
}

  
  return (
    <>
      <AppWrap   
        handleModal={handleModal}
        handleModalType={handleModalType}
      />
      {isModal && 
        <Modal
          modalContent={modalContent}
          closeModal={handleModal}
          handleOutside={handleOutside}
          modalType={modalType}
        />}
    </>
  )
}

export default App
