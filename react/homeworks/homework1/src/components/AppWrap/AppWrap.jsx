import { useState } from "react";
import Button from "../Button";
import { FIRST_MODAL, SECOND_MODAL } from "../../constants/nameConstants.js";

export default function AppWrap({handleModal, handleModalType}) {

  return (

    <>
      <h1>Homework 1. Modal</h1>
      <div className="btn-wrap">
        <Button
            btnTitle={FIRST_MODAL}
            closeModal={handleModal}
            type={handleModalType}
        />
        <Button
            btnTitle={SECOND_MODAL}
            closeModal={handleModal}
            type={handleModalType}
        />
      </div>
    </>
  );
}
