import {SECOND_MODAL} from "../../constants/nameConstants.js";
import cn from 'classnames';

export default function Button(props) {
  const { btnTitle, closeModal, type } = props;

  return (
    <button
      className={cn(
        {
            tomato: btnTitle === SECOND_MODAL
        }
      )}
      onClick={(e) => {
        type(e);
        closeModal();
      }}
    >
      {btnTitle}
    </button>
  );
}
