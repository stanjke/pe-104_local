import React, { FC, useState } from 'react'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import CartItemSummary from './CartItemSummary'
import { IFakeProducts } from '../../types/types';
import { useActions } from '../../hooks/useActions';
import CheckoutForm from '../CheckoutForm/CheckoutForm';

const Cart: FC = () => {

    // const { confirmOrder } = useActions()
    const [isFormOpen, setIsFormOpen] = useState(false)
    const cart = useTypedSelector(state => state.cart)
    const isEmpty: boolean = cart.length !== 0 ? false : true;


    const toggleCheckoutForm = () => {
        setIsFormOpen(!isFormOpen)
    }

    const handleCheckoutFormOutside: React.MouseEventHandler<HTMLDivElement | undefined>  = (event?) => {
        if(!event) {
            toggleCheckoutForm();
        } else if (!event.target.closest('.checkout__form')) {
            toggleCheckoutForm();
        }
    }
    

        return (

            <>
                <div className='fixed flex flex-col justify-between right-0 top-[50px] h-[100vh] w-[400px] bg-gray-100'>
                    <div className='overflow-auto'>
                        {cart.length !== 0 ?
                            cart?.map((p: IFakeProducts) => <CartItemSummary key={p.id} product={p} />) :
                            <div className='absolute top-[50%] left-[50%] -translate-y-1/2 -translate-x-1/2'>Cart is empty...</div>
                        }
                    </div>
                    <button

                        disabled={isEmpty}
                        onClick={toggleCheckoutForm}
                        className={`
                    ${isEmpty ? 'opacity-50 pointer-events-none' : ''}
                    bg-blue-500 
                    hover:bg-blue-700 
                    text-black 
                    cursor-pointer
                    font-bold 
                    py-2 
                    px-4 
                    rounded
                    mb-[75px] 
                    mt-[25px] 
                    mx-10 
                    `}>
                        Confirm Order
                    </button>
                    {isFormOpen && <CheckoutForm action={handleCheckoutFormOutside} cart={cart}/>}
                </div>

            </>



        )
    }

    export default Cart