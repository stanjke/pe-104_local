import { FC } from "react"
import { useActions } from "../../hooks/useActions"
import { IFakeProductsCardProps } from "../../types/types"


const CartItemSummary: FC<IFakeProductsCardProps> = ({ product }) => {

    const { removeFromCart, increaseQuantity, decreaseQuantity } = useActions()


    return (
        <>
            <div className="flex border p-4 mb-4 shadow-lg bg-gray-200">
                <div className="w-[64px] mr-4">
                    <img src={product.image} alt={product.title} className="w-16 h-16 object-cover rounded-full shadow-md float-left mr-4" />
                </div>

                <div className="flex flex-col">
                    <h2 className="font-medium text-base">{product.title}</h2>
                    <p className="text-lg font-semibold text-gray-700 mt-2">Price: ${product.price}</p>

                    <p>Count: {product.quantity}</p>
                    <button onClick={() => increaseQuantity(product)} className="border-2 w-20px bg-gray-500"> + </button>
                    <button onClick={() => decreaseQuantity(product)} className="border-2 w-20px bg-gray-500"> - </button>
                    <button onClick={() => removeFromCart(product)} className="text-red-500 hover:text-red-700 mt-2 self-end">
                        Remove
                    </button>

                </div>

            </div>
        </>



    )
}


export default CartItemSummary
