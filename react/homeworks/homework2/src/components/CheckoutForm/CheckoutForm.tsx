import React, { FC } from "react";
import { useActions } from "../../hooks/useActions";
import { Form, Formik } from "formik";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import InputBox from "./InputBox/InputBox.tsx";
import { ICheckoutForm, IFromData } from "../../types/types.ts";
import { validationSchema } from "./validation.ts";

const CheckoutForm: FC<ICheckoutForm> = (props) => {

    const {

        action: handleoutside,
        cart,

    } = props

    const { startedFormData, filledFormData } = useTypedSelector(state => state.checkout)

    const { checkout, confirmOrder } = useActions()

    const initialValues: IFromData | Record<string, never> = Object.keys(filledFormData).length !== 0 ? filledFormData : startedFormData;

    const handleFormOutside: React.MouseEventHandler<HTMLDivElement> = (e) => {
        handleoutside(e);
    }

    const userCartSummary = (): void => {
        let accumulator: number = 0;
        cart?.forEach(({ title, price, quantity }, index) => {

            if (quantity) {
                accumulator += price * quantity

                console.group(`Item number ${index + 1}: ${title}`);
                console.log(`Quantity: ${quantity}`);
                console.log(`Price: ${price * quantity} $`);
                console.groupEnd()

                if (index === cart.length - 1) {
                    console.group('Total order price: ')
                    console.log('Total order price: ', Math.floor(accumulator), '$');
                    console.groupEnd()

                }
            }
        })
    }

    const showFormData = ({ firstName, secondName, age, email, tel, shippingAddress }: IFromData): void => {
        console.group('User shipping info: ')
        console.log('User first name: ', firstName)
        console.log('User second name: ', secondName)
        console.log('User age: ', age)
        console.log('User email: ', email)
        console.log('User phone: ', tel)
        console.log('User address: ', shippingAddress)
        console.groupEnd()
    }

    const checkoutSummary = (values: IFromData): void => {
        checkout(values);
        handleFormOutside();
        userCartSummary();
        confirmOrder();
        showFormData(values)
    }


    return (
        <div
            onClick={(e) => handleFormOutside(e)}
            className="fixed inset-0 flex items-center justify-center z-50 bg-black/50"
        >
            <div className="checkout__form bg-slate-200 rounded-lg shadow-lg p-6 w-auto">
                <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={(values) => {
                        checkoutSummary(values);
                    }}>
                    {({ errors, touched }) => (
                        <Form>
                            <fieldset className="mb-4">
                                <legend className="text-lg font-semibold text-center mb-2">Checkout</legend>
                                <div className="mb-2">
                                    <InputBox
                                        name="firstName"
                                        label="First Name"
                                        placeholder="First name..."
                                        error={errors.firstName && touched.firstName}
                                    />
                                </div>
                                <div className="mb-2">
                                    <InputBox
                                        name="secondName"
                                        label="Second Name"
                                        placeholder="Second name..."
                                        error={errors.secondName && touched.secondName}
                                    />
                                </div>
                                <div className="mb-2">
                                    <InputBox
                                        name="email"
                                        label="Email"
                                        placeholder="example@mail.com"
                                        error={errors.email && touched.email}
                                    />
                                </div>
                                <div className="mb-2">
                                    <InputBox
                                        name="age"
                                        label="Age"
                                        placeholder="Enter you age..."
                                        error={errors.secondName && touched.secondName}
                                    />
                                </div>
                                <div className="mb-2">
                                    <InputBox
                                        name="shippingAddress"
                                        label="Address"
                                        placeholder="Delivery address..."
                                        error={errors.shippingAddress && touched.shippingAddress}
                                    />
                                </div>
                                <div className="mb-2">
                                    <InputBox
                                        name="tel"
                                        label="Telephone"
                                        placeholder="Telephone number..."
                                        error={errors.tel && touched.tel}
                                    />
                                </div>
                            </fieldset>
                            <button
                                className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600"
                                type="submit">
                                Confirm
                            </button>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>


    )
}

export default CheckoutForm