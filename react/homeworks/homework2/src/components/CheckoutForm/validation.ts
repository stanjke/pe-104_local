import * as Yup from 'yup'

export const validationSchema = Yup.object({
    firstName: Yup.string()
        .min(3, 'Must be atleast 3 characters')
        .max(15, 'Must be 15 characters or less')
        .required('Required'),
    secondName: Yup.string()
        .min(3, 'Must be atleast 3 characters')
        .max(15, 'Must be 15 characters or less')
        .required('Required'),
    email: Yup.string()
        .min(3, 'Must be atleast 3 characters')
        .email('Must be valid email')
        .required('Required'),
        age: Yup.number()
        .min(18)
        .max(120, 'Must be less then 120')
        .required('Required'),
    shippingAddress: Yup.string()
        .min(3, 'Must be atleast 3 characters')
        .max(15, 'Must be 15 characters or less')
        .required('Required'),
    tel: Yup.string()
        .min(3, 'Must be atleast 3 characters')
        .max(15, 'Must be 15 characters or less')
        .required('Required'),

})