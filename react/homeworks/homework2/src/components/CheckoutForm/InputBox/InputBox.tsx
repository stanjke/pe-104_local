import { ErrorMessage, Field } from 'formik'
import React, { FC } from 'react'
import { IInputBoxProps } from '../../../types/types'

const InputBox: FC<IInputBoxProps> = (props) => {
    const {
        name,
        label,
        placeholder,
        type,
        error,
        ...restProps
    } = props
  return (
    <label className='relative'>
        <p>{label}</p>
        <Field 
        className={'w-[300px] p-1.5 rounded mb-2'} 
        name={name} 
        type={type} 
        placeholder={placeholder} {...restProps}/>
        <ErrorMessage name={name} className='absolute left-0 bottom-[-30px] text-red-500 text-sm' component={'p'}/>
    </label>
  )
}


InputBox.defaultProps = {
    type: 'text'
}

export default InputBox