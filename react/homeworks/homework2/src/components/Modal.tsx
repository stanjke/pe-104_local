import { FC } from "react";
import { useActions } from "../hooks/useActions";
import { ModalProps } from "../types/types";


const Modal: FC<ModalProps> = ({ action, actionParam }) => {

    const { toggleModal } = useActions()

    const handleModalOutside: React.MouseEventHandler<HTMLDivElement> = (event) => {
        if (!event.target.closest('.modal')) {
            toggleModal();
        }
    };


    return (
        <div onClick={(e) => handleModalOutside(e)} className="fixed inset-0 flex items-center justify-center z-50 bg-black/5">
            <div className="modal bg-white rounded-lg shadow-lg p-6">
                <h2 className="text-lg font-semibold mb-4">{ }</h2>
                <p className="mb-4">Вы уверены, что хотите добавить товар в корзину?</p>
                <div className="flex justify-end space-x-2">
                    <button
                        onClick={() => {
                            action(actionParam)
                            toggleModal()
                        }}
                        className="bg-green-500 text-white px-4 py-2 rounded hover:bg-green-600"

                    >
                        Yes
                    </button>
                    <button
                        onClick={() => toggleModal()}
                        className="bg-red-500 text-white px-4 py-2 rounded hover:bg-red-600"

                    >
                        Отмена
                    </button>
                </div>
            </div>
        </div>
    );
};
export default Modal;
