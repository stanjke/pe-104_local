import { FC } from 'react'
import { IFakeProductsCardProps } from '../types/types';
import { ReactComponent as FavEmpty } from '../assets/fav_empty.svg'
import { ReactComponent as FavFill } from '../assets/fav_fill.svg'
import { ReactComponent as CartAdd } from '../assets/cart_add.svg'
import { ReactComponent as CartAdded } from '../assets/cart_added.svg'
import { useActions } from '../hooks/useActions';
import { useTypedSelector } from '../hooks/useTypedSelector';
import Modal from './Modal';

const ProductCard: FC<IFakeProductsCardProps> = ({ product, status }) => {

    const cart = useTypedSelector(state => state.cart)
    const favorites = useTypedSelector(state => state.favorites)
    const { isOpen } = useTypedSelector(state => state.modal)
    const selectedProduct = useTypedSelector(state => state.selectedProduct)
    const {
        toggleFavorites,
        addToCart,
        toggleModal,
        setSelectedProduct,
    } = useActions()

    const handleFavorites: React.MouseEventHandler = (e): void => {
        e.preventDefault()
        toggleFavorites(product)
    }

    const handleCart = (): void => {
        toggleModal()
        setSelectedProduct(product)
    }

    const isFavoriteExist: boolean = favorites.some((p: { id: number; }) => p.id === product.id)
    const isCartExist: boolean = cart.some((p: { id: number; }) => p.id === product.id)

    return (
        <>
            {status ?
                <div className='m-5 w-[270px] flex-col justify-between' key={product.id} data-testid="col">
                    <div className='relative mt-[20px]'>
                        <img
                            className='w-full h-[350px] object-contain rounded-lg'
                            src={product.image}
                            alt={product.title}
                        />
                        <a onClick={(e) => handleFavorites(e)} className='absolute top-[-20px] right-[20px]' href="#">
                            {isFavoriteExist ? <FavFill fill='orange' /> : <FavEmpty fill='orange' />}
                        </a>
                    </div>
                    <div className='flex justify-between items-center'>
                        <div className='flex flex-col gap-2 mt-3 mb-3 justify-between'>
                            <h5 className='h-[100px]'>{product.title}</h5>
                            <p className='font-bold text-3xl'>{Math.floor(product.price)}<span className='text-sm'> $</span></p>
                        </div>
                        <div className='product-details mr-5'>
                            <button
                                disabled={isCartExist ? true : false}
                                onClick={handleCart}
                                className="flex items-center gap-2 bg-transparent text-white py-2 px-4 rounded"
                            >
                                {isCartExist ? <CartAdded fill='gray' /> : <CartAdd fill='lightgreen' />}
                            </button>
                        </div>
                    </div>
                </div>
                :
                <div className='m-5 w-[50%] flex justify-between relative' key={product.id} data-testid="row">
                    <div className=''>
                        <img
                            className='w-[200px] h-[200px] object-contain rounded-lg'
                            src={product.image}
                            alt={product.title}
                        />
                        <a onClick={(e) => handleFavorites(e)} className='absolute top-[-20px] right-[20px]' href="#">
                            {isFavoriteExist ? <FavFill fill='orange' /> : <FavEmpty fill='orange' />}
                        </a>
                    </div>
                    <div className='flex flex-col gap-2 mt-3 mb-3'>
                        <h5>{product.title}</h5>
                        <p className='text-lg font-semibold'>{Math.floor(product.price)}<span className='text-sm'> $</span></p>
                    </div>
                    <div className='product-details'>
                        <button
                            disabled={isCartExist ? true : false}
                            onClick={handleCart}
                            className="flex items-center gap-2 bg-transparent text-white py-2 px-4 rounded"
                        >
                            {isCartExist ? <CartAdded fill='gray' /> : <CartAdd fill='lightgreen' />}
                        </button>
                    </div>
                </div>
            }
            {isOpen && <Modal action={addToCart} actionParam={selectedProduct} />}
        </>

    );
}

export default ProductCard;
