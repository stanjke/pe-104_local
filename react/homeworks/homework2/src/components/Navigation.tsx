import { FC, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useDebounce } from '../hooks/useDebounce'
import { ReactComponent as CartFill } from '../assets/cart_fill.svg'
import { ReactComponent as CartEmpty } from '../assets/cart_empty.svg'
import { ReactComponent as FavFill } from '../assets/fav_fill.svg'
import { ReactComponent as FavEmpty } from '../assets/fav_empty.svg'
import { useTypedSelector } from '../hooks/useTypedSelector'
import { useActions } from '../hooks/useActions'

const Navigation: FC = () => {
    const cart = useTypedSelector(state => state.cart)
    const favorites = useTypedSelector(state => state.favorites)

    const { toggleCart } = useActions()
    const [search, setSearch] = useState<string>('')
    const debounced = useDebounce(search)

    useEffect(() => {
        console.log(debounced);
    }, [debounced])

    return (
        <nav className='fixed top-0 left-0 w-[100%] z-10 flex justify-between items-center h-[50px] px-5 shadow-md bg-gray-500 text-white'>
            <h1>
                <a className='font-bold' href="#">logo</a>
            </h1>
            <ul className='flex gap-2'>
                <li>
                    <Link to='/'>Shop</Link>
                </li>
                <li>
                    <Link to='/favorites'>Favorites</Link>
                </li>
            </ul>
            <span className='relative'>
                <input
                    type="text"
                    placeholder='Search...'
                    className='border py-1 px-2 text-black h-[30px] '
                    value={search}
                    onChange={e => setSearch(e.target.value)}
                />
            </span>
            <span className='flex justify-center items-center min-w-[60px]'>
                <Link className='relative mr-2 ' to='/favorites'>
                    {favorites.length ?
                        <>
                            <FavFill fill='white' />
                            <span className='absolute w-3 top-[40%] left-[-10%] text-xs bg-red-600 rounded-full text-center' >
                                {favorites.length}
                            </span>
                        </>
                        :
                        <FavEmpty fill='white' />
                    }
                </Link>
                <button onClick={() => toggleCart()} className="p-0 m-0 border-none bg-transparent text-white cursor-pointer relative">
                    {cart.length ?
                        <>
                            <CartFill fill='white' />
                            <span className='absolute w-3 top-[40%] left-[-10%] text-xs bg-red-600 rounded-full text-center' >
                                {cart.reduce((acc, product) => product.quantity ? acc + product.quantity : 0, 0)}
                            </span>
                        </>
                        : <CartEmpty fill='white' />
                    }
                </button>
            </span>
        </nav>
    )
}


export default Navigation