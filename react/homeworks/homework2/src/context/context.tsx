import { createContext, useState } from 'react'
import { IReactNode } from '../types/types'

export const ProductViewContext = createContext({})

const initialState: boolean = true

const ProductViewProvider = ({ children }: IReactNode) => {
	const [productViewStore, setProductViewStore] = useState<boolean>(initialState)

	const toggleProductView = () => setProductViewStore(!productViewStore)

	const getProductViewStatus = () => productViewStore

	return (
		<ProductViewContext.Provider value={{ productViewStore, toggleProductView, getProductViewStatus }}>
			{children}
		</ProductViewContext.Provider>
	)
}



export default ProductViewProvider;
