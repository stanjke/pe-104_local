import { ActionCreatorsMapObject, bindActionCreators } from "@reduxjs/toolkit";
import { useMemo } from "react";
import { useDispatch } from "react-redux";
import { favoritesActions } from "../store/favorites/favoritesSlise";
import { cartActions } from "../store/cart/cartSlice";
import { modalActions } from "../store/modal/modalSlice";
import { selectedProductActions } from "../store/selectedProduct/selectedProduct";
import { isCartOpenActions } from "../store/cart/isCartOpenSlise";
import { checkoutAction } from "../store/checkoutForm/checkoutForm";

const rootActions = {
    ...checkoutAction,
    ...favoritesActions,
    ...cartActions,
    ...modalActions,
    ...selectedProductActions,
    ...isCartOpenActions,
}


export const useActions = () => {
    const dispatch = useDispatch()

    return useMemo(() => bindActionCreators(rootActions, dispatch), [dispatch]);
}