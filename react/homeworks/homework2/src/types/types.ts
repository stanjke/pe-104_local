import { ReactNode } from "react"

export interface IProducts {
  name: string
  price: number
  image: string
  id: number
  color: string
  category: string
}

export interface IProductCardProps {
  product: IProducts;
  key: number
}


export interface IFakeProducts {
  id: number
  title: string
  price: number
  description: string
  category: string
  image: string
  rating: IFakeProductsRating
  quantity?: number
}

interface IFakeProductsRating {
  rate: number
  count: number
}

export interface IFakeProductsCardProps {
  product: IFakeProducts,
  key: number,
  status: boolean,
}

export interface ModalProps {
  action: (param: any) => void;
  actionParam: IFakeProducts | null;
}

export interface ICart {
  products: IFakeProducts[]
  isOpen: boolean
}

export interface IInputBoxProps {
  name: string,
  label: string,
  placeholder?: string,
  type?: string,
  error?: {},
  restProps?: Record<string, any>,
}

export interface ICheckoutForm {
  action: (param: any) => void;
  cart: IFakeProducts[]
}

export interface IFormDataStore {
  startedFormData: IFromData;
  filledFormData: Record<string, never> | IFromData;
}


export interface IFromData {
  firstName?: string,
  secondName?: string,
  email?: string,
  age?: string,
  shippingAddress?: string,
  tel?: string,
}

export interface IReactNode {
  children: ReactNode;
}