import React, { FC } from 'react'
import { useTypedSelector } from '../hooks/useTypedSelector'
import { IFakeProducts } from '../types/types'
import ProductCard from '../components/ProductCard'




const FavouritesPage: FC = () => {

  const favorites = useTypedSelector(state => state.favorites)

  return (
    <>
      <main className='container mx-auto mt-[50px] flex justify-center flex-wrap box-border'>
        {favorites.length === 0 && <h1 className='text-red-700 text-2xl mt-[200px]'>There are no favorites yet...</h1>}
        {
          favorites?.map(
            (product: IFakeProducts) =>
              <ProductCard
                key={product.id}
                product={product}
                status={true}
              />)
        }
      </main>
    </>
  )
}


export default FavouritesPage