import { FC, useContext } from 'react';
// import { useProductsQuery } from '../store/serverRespond/toLocalJsonFile'
import ProductCard from '../components/ProductCard';
import { IFakeProducts } from '../types/types';
import { useGetAllProductsQuery } from '../store/serverRespond/fakeStoreApi';
import { ReactComponent as Tail } from '../assets/tail_list_fill.svg'
import { ReactComponent as Grid } from '../assets/grid-fill.svg'
import { ProductViewContext } from '../context/context.tsx'


const ShopPage: FC = () => {
  // const { data } = useProductsQuery()  // HOOK TO LOCAL .JSON FILE...
  const { data: products, isLoading } = useGetAllProductsQuery()
  const { productViewStore, toggleProductView } = useContext(ProductViewContext)

  return (
    <>
      {productViewStore ?
        <main className='container mx-auto mt-[70px] flex flex-col justify-center flex-wrap box-border'>
          <div className=' h-[30px] flex gap-2 justify-end items-center'>
            <Grid className='rounded-2xl' cursor='pointer' fill='#BFDBFE90' data-testid='grid' />
            <Tail onClick={toggleProductView} cursor='pointer' fill='#6B7280' />

          </div>
          <div className='mx-auto flex justify-center flex-wrap box-border'>
            {isLoading && <h1 className='text-red-700 text-2xl mt-[200px]'>Loading...</h1>}
            {productViewStore &&
              products?.map(
                (product: IFakeProducts) =>
                  <ProductCard
                    key={product.id}
                    product={product}
                    status={productViewStore}
                  />)
            }
          </div>
        </main>
        :
        <main className='container mx-auto mt-[70px] flex flex-col justify-center flex-wrap box-border'>
          <div className=' h-[30px] flex gap-2 justify-end items-center'>
            <Grid onClick={toggleProductView} cursor='pointer' fill='#6B7280' />
            <Tail className='rounded-2xl' cursor='pointer' fill='#BFDBFE90' data-testid='tail' />
          </div>
          <div className='flex flex-col justify-center box-border items-center'>
            {isLoading && <h1 className='text-red-700 text-2xl mt-[200px]'>Loading...</h1>}
            {products?.map(
              (product: IFakeProducts) =>
                <ProductCard
                  key={product.id}
                  product={product}
                  status={productViewStore}
                />)
            }
          </div>
        </main>
      }
    </>
  )
}

export default ShopPage