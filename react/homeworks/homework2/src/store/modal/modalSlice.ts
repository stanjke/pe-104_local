import { createSlice } from "@reduxjs/toolkit";

export const modalSlice = createSlice({
    name: 'modal',
    initialState: {
        isOpen: false
    },
    reducers: {
        toggleModal: (state) => {
            state.isOpen = !state.isOpen;
        }
    }
})


export const { actions: modalActions, reducer: modalSliceReducer } = modalSlice