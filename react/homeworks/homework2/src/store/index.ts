import { combineReducers, configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { toLocalJsonFile } from "./serverRespond/toLocalJsonFile";
import { fakeStoreApi } from "./serverRespond/fakeStoreApi";
import { favoritesSliceReducer } from "./favorites/favoritesSlise";
import { cartSliceReducer } from "./cart/cartSlice";
import { modalSliceReducer } from "./modal/modalSlice";
import { selectedProductReducer } from "./selectedProduct/selectedProduct";
import { isCartOpenSliceReducer } from "./cart/isCartOpenSlise";
import { checkoutFormSliceReducer } from "./checkoutForm/checkoutForm";


const rootReducer = combineReducers({
    checkout: checkoutFormSliceReducer,
    selectedProduct: selectedProductReducer,
    modal: modalSliceReducer,
    cart: cartSliceReducer,
    isCartOpen: isCartOpenSliceReducer,
    favorites: favoritesSliceReducer,
    [toLocalJsonFile.reducerPath]: toLocalJsonFile.reducer,
    [fakeStoreApi.reducerPath]: fakeStoreApi.reducer,
})

export const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware().concat(toLocalJsonFile.middleware).concat(fakeStoreApi.middleware),
})


export type RootState = ReturnType<typeof store.getState>;