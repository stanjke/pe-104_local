import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { IFakeProducts } from "../../types/types";

const favoritesFromStorage = localStorage.getItem('favorites');

const localFavorites: IFakeProducts[] = favoritesFromStorage ? JSON.parse(favoritesFromStorage) : [];

const initialState: IFakeProducts[] = localFavorites;

export const favoritesSlice = createSlice({
    name: 'favorites',
    initialState,
    reducers: {
        toggleFavorites: (state, { payload: product }: PayloadAction<IFakeProducts>) => {

            const isExist: boolean = state.some(p => p.id === product.id);

            if (isExist) {
                const index: number = state.findIndex(item => item.id === product.id)
                if (index !== -1) {
                    state.splice(index, 1)
                    localStorage.setItem(`${favoritesSlice.name}`, JSON.stringify(state))
                }
            } else {
                state.push(product)
                localStorage.setItem(`${favoritesSlice.name}`, `${JSON.stringify(state)}`)
            }

        }
    }
})

export const { actions: favoritesActions, reducer: favoritesSliceReducer } = favoritesSlice