import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { IProducts } from '../../types/types'

export const toLocalJsonFile = createApi({
    reducerPath: 'toLocalJsonFile/api',
    baseQuery: fetchBaseQuery({
        baseUrl: ''
    }),
    endpoints: build => ({
        products: build.query<IProducts[], number>({
            query: () => '/products.json',
        })
    })
})

export const { useProductsQuery } = toLocalJsonFile