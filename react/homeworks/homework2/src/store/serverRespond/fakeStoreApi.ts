import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";
import { IFakeProducts } from "../../types/types";

export const fakeStoreApi = createApi({
    reducerPath: 'fakeStoreApi',
    baseQuery: fetchBaseQuery({
        baseUrl: "https://fakestoreapi.com"
    }),
    endpoints: builder => ({
        getAllProducts: builder.query<IFakeProducts[], void>({
            query: () => '/products'
        })
    })
})

export const { useGetAllProductsQuery } = fakeStoreApi