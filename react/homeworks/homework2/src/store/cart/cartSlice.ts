import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { ICart, IFakeProducts } from "../../types/types";


const cartFromStorage = localStorage.getItem('cart');

const localCart: IFakeProducts[] = cartFromStorage ? JSON.parse(cartFromStorage) : [];

const initialState = localCart;


export const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, { payload: product }: PayloadAction<IFakeProducts>) => {

            const isExist: boolean = state.some(p => p.id === product.id);

            if (!isExist) {
                state.push({ ...product, quantity: 1 });
                localStorage.setItem(`${cartSlice.name}`, JSON.stringify(state));
            }
        },
        removeFromCart: (state, { payload: product }: PayloadAction<IFakeProducts>) => {
            const index = state.findIndex(item => item.id == product.id);
            state.splice(index, 1);
            localStorage.setItem(`${cartSlice.name}`, JSON.stringify(state));
        },
        increaseQuantity: (state, { payload: product }) => {
            const index = state.findIndex(item => item.id == product.id);
            const productQuantity = product.quantity
            state.splice(index, 1, { ...product, quantity: productQuantity + 1 });
            localStorage.setItem(`${cartSlice.name}`, JSON.stringify(state));
        },
        decreaseQuantity: (state, { payload: product }) => {
            const index = state.findIndex(item => item.id == product.id);
            const productQuantity = product.quantity
            if (productQuantity <= 1) return
            state.splice(index, 1, { ...product, quantity: productQuantity - 1 });
            localStorage.setItem(`${cartSlice.name}`, JSON.stringify(state));
        },
        confirmOrder: (state) => {
            state.length = 0;
            localStorage.setItem(`${cartSlice.name}`, JSON.stringify(state));
        }
    }
})

export const { actions: cartActions, reducer: cartSliceReducer } = cartSlice;