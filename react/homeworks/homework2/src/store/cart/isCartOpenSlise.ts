import { createSlice } from "@reduxjs/toolkit";



const initialState: boolean = false


export const isCartOpenSlise = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        toggleCart: (state => state = !state)
    }
})

export const { actions: isCartOpenActions, reducer: isCartOpenSliceReducer } = isCartOpenSlise;