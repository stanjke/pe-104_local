import { createSlice } from "@reduxjs/toolkit";
import { IFormDataStore, IFromData } from "../../types/types";


const initialState: IFormDataStore = {
    startedFormData: {
        firstName: '',
        secondName: '',
        email: '',
        age: '',
        shippingAddress: '',
        tel: '',
    },
    filledFormData: {}
}

export const checkoutFormSlice = createSlice({
    name: 'checkoutForm',
    initialState,
    reducers: {
        checkout: (state, { payload }) => {
            state.filledFormData = { ...payload }
        }
    },
}
)


export const { actions: checkoutAction, reducer: checkoutFormSliceReducer } = checkoutFormSlice