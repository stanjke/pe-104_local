import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { IFakeProducts } from "../../types/types";


export const selectedProductSlice = createSlice({
    name: "selectedProduct",
    initialState: null as IFakeProducts | null,
    reducers: {
        setSelectedProduct: (state, action: PayloadAction<IFakeProducts>) => action.payload,
        clearSelectedProduct: () => null,
    },
});

export const { actions: selectedProductActions, reducer: selectedProductReducer } = selectedProductSlice