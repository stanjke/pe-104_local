import React, { FC } from "react"
import { Routes, Route } from 'react-router-dom'
import ShopPage from "./pages/ShopPage"
import FavouritesPage from "./pages/FavouritesPage"
import Navigation from "./components/Navigation"
import Cart from "./components/Cart/Cart"
import { useTypedSelector } from "./hooks/useTypedSelector"
import ProductViewProvider from "./context/context"


const App: FC = () => {

  const isCartOpen = useTypedSelector(state => state.isCartOpen)

  return (
    <>
      <Navigation />
      <ProductViewProvider>
        <Routes>
          <Route path="/" element={<ShopPage />} />
          <Route path="/favorites" element={<FavouritesPage />} />
        </Routes>
      </ProductViewProvider>
      {isCartOpen && <Cart />}
    </>

  )
}

export default App
