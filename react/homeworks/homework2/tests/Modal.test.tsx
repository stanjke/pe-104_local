import { render, screen, fireEvent } from '@testing-library/react';
import { store } from '../src/store/index'
import { Provider } from 'react-redux/es/exports'
import Modal from '../src/components/Modal';
import { describe, expect, it, vitest } from 'vitest';
import React from 'react';

const handleClick = vitest.fn()

describe('Testing Modal component', () => {
    it('render Modal component', () => {
        const { getByText } = render(
            <Provider store={store}>
                <Modal />
            </Provider>
        )
        expect(screen.getByText('Yes')).toBeInTheDocument()
    })
    it('button click', () => {
        const { container } = render(
            <Provider store={store}>
                <Modal action={handleClick} />
            </Provider>
        )
        const button = container.getElementsByTagName('button')[0]
        fireEvent.click(button)
        expect(handleClick).toHaveBeenCalled()
    })

})