import { render, screen, waitFor } from '@testing-library/react';
import { store } from '../src/store/index'
import { Provider } from 'react-redux/es/exports'
import ShopPage from '../src/pages/ShopPage';
import { describe, expect } from 'vitest';
import React from 'react';
import ProductViewProvider from '../src/context/context';


describe('Testing ShopPage component', () => {
    it('render with fetching data', async () => {
        const { getByText } = render(
            <Provider store={store}>
                <ShopPage />
            </Provider>
        )
        await waitFor(() => {
            expect(getByText("Mens Casual Premium Slim Fit T-Shirts")).toBeInTheDocument()
        })
    })
})