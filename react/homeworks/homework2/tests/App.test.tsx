import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom'
import { store } from '../src/store/index'
import { Provider } from 'react-redux/es/exports'
import App from '../src/App';
import { expect } from 'vitest';
import React from 'react';

describe('Testing App component', () => {
  it('render App', () => {
    const app = render(<Provider store={store}><BrowserRouter><App /></BrowserRouter></Provider>)
    expect(app).toBeInTheDocument
  });
  it('Children components render', () => {
    render(<Provider store={store}><BrowserRouter><App /></BrowserRouter></Provider>)
    expect(screen.getByText('Shop')).toBeInTheDocument
  })
});