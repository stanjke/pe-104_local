import { render, screen } from '@testing-library/react';
import { store } from '../src/store/index'
import { Provider } from 'react-redux/es/exports'
import ProductCard from '../src/components/ProductCard';
import { describe, expect } from 'vitest';
import React from 'react';

const mockProps = {
    product: {
        category: "men's clothing",
        description: "Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday",
        id: 1,
        image: "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
        price: 109.95,
        rating: {
            rate: 3.9,
            count: 120
        },
        title: "expected props founded"
    },
    emptyProduct: {},
    status: true
}

describe('Testing ProductCard component', () => {
    it('renders with expected props', () => {
        render(
            <Provider store={store}>
                <ProductCard
                    product={mockProps.product}
                    status={mockProps.status} />
            </Provider>
        )
        expect(screen.getByText('expected props founded')).toBeInTheDocument
    })
    it('render correct structure depend on prop status true', () => {

        const expectedClass = 'm-5 w-[270px] flex-col justify-between'

        const { getByTestId } = render(
            <Provider store={store}>
                <ProductCard
                    product={mockProps.product}
                    status={mockProps.status}
                />
            </Provider>
        )
        const element = getByTestId('col')
        expect(element).toHaveAttribute('class', expectedClass)

    })
    it('render correct structure depend on prop status false', () => {

        const expectedClass = 'm-5 w-[50%] flex justify-between relative'

        const { getByTestId } = render(
            <Provider store={store}>
                <ProductCard
                    product={mockProps.product}
                    status={!mockProps.status} />
            </Provider>
        )
        const element = getByTestId('row')
        expect(element).toHaveAttribute('class', expectedClass)

    })
})