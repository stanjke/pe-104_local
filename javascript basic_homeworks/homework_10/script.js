//вытаскиваю из UL все лишки масивом, по тому, что если у меня будут добавлятся новые элементы,
//то мне не важно сколлько их там будет я буду рабоать циклом в массиве элементов.
const tabsList = document.querySelector(".tabs");
const tabs = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelector(".tabs-content");
let tabsChildren = Array.from(tabs);
let tabsContentChildren = Array.from(tabsContent.children);

//получил индекс элемента на на котором висит актив по умолчанию
let pTarget = indexActiveTab(tabsChildren);

//использовал этот индекс для того что бы скрыть все параграфы которые не соответствуют моему активному из верхнего списка
setActive(tabsContentChildren);

//скрыл все параграфы у которых нету класса актив
hideTabs(tabsContentChildren);

tabsList.addEventListener("click", function (event) {
  removeActive(tabsChildren);
  event.target.classList.add("active");
  pTarget = indexActiveTab(tabsChildren);
  setActive(tabsContentChildren);
  hideTabs(tabsContentChildren);
  showTab(tabsContentChildren);
});

function indexActiveTab(arr) {
  let elemIndex = null;
  arr.forEach((elem, index) => {
    if (elem.classList.contains("active")) {
      elemIndex = index;
      return;
    }
  });
  return elemIndex;
}

function hideTabs(arr) {
  for (elem of arr) {
    if (elem.classList.contains("active")) {
      continue;
    }
    elem.style.display = "none";
  }
}

function showTab(arr) {
  for (elem of arr) {
    if (elem.classList.contains("active")) {
      elem.style.display = "block";
    } else {
      continue;
    }
  }
}

function removeActive(arr) {
  for (elem of arr) {
    elem.classList.remove("active");
  }
}

function setActive(arr) {
  removeActive(arr);
  for (let i = 0; i < arr.length; i++) {
    if (pTarget === i) {
      arr[i].classList.add("active");
    } else {
      continue;
    }
  }
}
