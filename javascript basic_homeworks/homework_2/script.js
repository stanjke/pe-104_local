// ТЕОРЕТИЧНІ ПИТАННЯ:

//1. Які існують типи даних у Javascript?
//2. У чому різниця між == і ===?
//3. Що таке оператор?

//1. В javascript существует 8 типов данных:
//      - string;
//      - number;
//      - boolean;
//      - bigint;
//      - undefined;
//      - object;
//      - symbol;
//      - null;
//2. Более одного занака = в javascript расценивается как сравнение. Есть два типа сравнения:
//      - не строгое сравнение "==" - это когда мы сравниваем значения операндов. Например:
//      console.log("5" == 5) вернет нам true. По тому что мы сравниваем значения 5 и 5.
//      - строгое сравнение "===" - это когда мы сравниваем и значение и тип операндов. Например:
//      console.log("5" === 5) вернет нам false. По тому что мы сперва сравниваем значения 5 и 5,
//      а после этого тип <string> == <number>  => false.
//3. Операторы это символы которые нужны для выполнения операций над операндами.
//   Как пример можно взять арифметические операторы: "+", "-", "*", "/".

// ЗАВДАННЯ

//1.Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера - alert, prompt, confirm. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//  Технічні вимоги:
//     Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
//     Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
//     Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача. Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
//     Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
//     Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
//     Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).

let userName = prompt("Please enter your name:");
let userAge = +prompt("Please enter your age:");

while (Boolean(userName) === false || isNaN(userAge) === true) {
  userName = prompt("Please re-enter your name:", userName);
  userAge = +prompt("Please re-enter your number:", userAge);
}

if (userAge < 18) {
  alert("You are not allowed to visit this website!");
} else if (userAge >= 18 && userAge <= 22) {
  let confirmation = confirm("Are you sure you want to continue?");
  if (confirmation == true) {
    alert(`Welcome, ${userName}!`);
  } else {
    alert("You are not allowed to visit this website!");
  }
} else {
  alert(`Welcome, ${userName}!`);
}
