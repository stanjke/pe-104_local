// ТЕОРЕТИЧНІ ПИТАННЯ:

// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// 1. Document Object Model это представление структуры html в виде объектов (узлов и нод) а так же
// включает методы и свойства для манипулирования узлами и элементами по средствам javascript.
// 2. Разница innerHTML и innerText в том что с помощью первого мы можем заменить содержимаое тега и полностью тег вместе с содержимым,
// а innerText изменяет только текстовую часть тега.
// 3. Обратить к элементу html можно несколькими способами:
// - .querySelector - возвращает первый элемент который совпал
// - .quetySelectorAll - возвращает коллекцию эллементов
// - .getElementByID - возвращает первый элемент который совпал
// - .getElementsByClassName - возвращает коллекцию эллементов
// - .getElementsByName - возвращает коллекцию эллементов
// - .getElementsByTagName - возвращает коллекцию эллементов
// Это основные способы обратиться к элементу DOM с помощью Javascript. Если я правильно понял то лучше те которые возвращают статическую коллекцию (элемент),
// и плюс первые два (.querySelector / .quetySelectorAll) более универсальны, по тому что синтаксис как css (.querySelector(#id)/.querySelector(.class)...) а так же
// они короче пишуться.
// ЗАВДАННЯ:

// Код для завдань лежить в папці project.
// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph
// Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з
// елементів присвоїти новий клас nav-item.
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

// let paragraph = document.querySelectorAll("p");

const paragraph = Array.from(document.querySelectorAll("p"));
paragraph.forEach((value) => (value.style.color = "#ff0000"));

//***********//

const optList = document.querySelector("#optionsList");
console.log(optList);

const optListParent = optList.parentElement;
console.log(optListParent);

const optListChildrenNodes = Array.from(document.querySelector("#optionsList").childNodes);
optListChildrenNodes.forEach((value) => console.log(`Node name - '${value.nodeName}' and node type - '${value.nodeType}'`));

//***********//

const testParagraph = document.querySelector("#testParagraph");
testParagraph.innerText = "This is a paragraph";

//***********//

const mainHeader = document.querySelector(".main-header");
const mainHeaderChildren = mainHeader.children;
console.log(mainHeaderChildren);

// Array.from(mainHeaderChildren).forEach((child) => child.classList.add("nav-item"));
// console.log(mainHeaderChildren);

Array.from(mainHeaderChildren).forEach((child) => {
  arrayOfClassNames = child.className.split(" ");
  if (!arrayOfClassNames.includes("nav-item")) {
    arrayOfClassNames.push("nav-item");
  }
  modifiedClassNameProp = arrayOfClassNames.join(" ");
  child.className = modifiedClassNameProp;
});
console.log(mainHeaderChildren);

//***********//

const removeSectionTitle = document.querySelectorAll(".section-title");
console.log(removeSectionTitle);

Array.from(removeSectionTitle).forEach((element) => element.classList.remove("section-title"));
console.log(removeSectionTitle);

//***********//
