let inputPass = document.querySelector("#password");
let confirmPass = document.querySelector("#confirmPassword");
let confirmBtn = document.querySelector("button");
let icons = document.querySelectorAll(".fa-eye");
let slashedIcons = document.querySelectorAll(".fa-eye-slash");
let passForm = document.querySelector(".password-form");

passForm.addEventListener("click", function (event) {
  if (event.target.classList.contains("input-icon") && !event.target.classList.contains("hidden")) {
    event.target.classList.toggle("hidden");
    if (event.target.classList.contains("fa-eye")) {
      for (elem of slashedIcons) {
        if (elem.classList.contains("input-icon")) {
          elem.classList.toggle("hidden");
          inputPass.type = "text";
        }
      }
    }
    if (event.target.classList.contains("fa-eye-slash")) {
      for (elem of icons) {
        if (elem.classList.contains("input-icon")) {
          elem.classList.toggle("hidden");
          inputPass.type = "password";
        }
      }
    }
  }
  if (event.target.classList.contains("confirm-icon") && !event.target.classList.contains("hidden")) {
    event.target.classList.toggle("hidden");
    if (event.target.classList.contains("fa-eye")) {
      for (elem of slashedIcons) {
        if (elem.classList.contains("confirm-icon")) {
          elem.classList.toggle("hidden");
          confirmPass.type = "text";
        }
      }
    }
    if (event.target.classList.contains("fa-eye-slash")) {
      for (elem of icons) {
        if (elem.classList.contains("confirm-icon")) {
          elem.classList.toggle("hidden");
          confirmPass.type = "password";
        }
      }
    }
  }
});

confirmBtn.addEventListener("click", function (event) {
  event.preventDefault();
  let errorOutput = document.createElement("p");
  let isExist = document.querySelector(".error");
  if (inputPass.value === confirmPass.value && inputPass.value !== "" && confirmPass.value !== "") {
    alert("You are welcome!");
    inputPass.value = "";
    confirmPass.value = "";
    if (isExist !== null) {
      isExist.remove();
    }
  } else {
    if (isExist === null) {
      errorOutput.classList.add("error");
      errorOutput.innerText = "Need to enter similar values!";
      errorOutput.style.color = "red";
      confirmBtn.before(errorOutput);
    }
  }
});
