// ТЕОРЕТИЧНІ ПИТАННЯ:

// 1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// 2.Які засоби оголошення функцій ви знаєте?
// 3.Що таке hoisting, як він працює для змінних та функцій?

// 1. Экранирование в Javascript - это способ обозначения специальных символов в строках, которые в обычном случае были бы интерпретированы как часть синтаксиса языка.
// 2. В Javascript существуюет 2 вида объявления функций. 1й - это function diclaration, 2й - это function expration.
// Разница между этиму двумя видами в том, что при объявлении функции с помощью function diclaration (ключевого слова function)
// у нас появляется возможность вызывать функцию даже до ее объявления. (сперва вызвать где-то вверху кода, а потом объясвить функцию в самом низу кода),
// при объявлении же через function expration (присваивание функции в переменную) мы ее можем вызвать только после того как объявили.
// Еще одним ключевым отличием между этими двумя видами объявления функций есть то, что при использовании function diclaration у нас есть возможность
// использовать масивоподобный объект "arguments", который покажет нам список передаваемых параметров. Для function expration же, такого встроенного функцианала нет,
// но есть другой, который доступен так же для function diclaration - это "...rest" оператор.
// 3. Hoisting как раз отвечает за "поднятие" в области видимости у функций и переменных в области видимости перед тем как будет выполнен код. На сколько я понял,
// он присущь для function diclaration и переменных объявленных ключевым словом "var".

// ЗАВДАННЯ:

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому
//  Javascript без використання бібліотек типу jQuery або React.
// Технічні вимоги:

//     Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
//         При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
//         Створити метод getAge() який повертатиме скільки користувачеві років.
//         Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі)
//         та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
//         Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

function createNewUser() {
  const newUser = {
    firstName: null,
    lastName: null,
    birthday: null,
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    setFirstName(value) {
      Object.defineProperty(this, "firstName", {
        writable: true,
      });
      this.firstName = value;
    },
    setLastName(value) {
      Object.defineProperty(this, "lastName", {
        writable: true,
      });
      this.lastName = value;
    },
    getAge() {
      let dateNow = new Date().getTime();
      let userAge = Math.floor((dateNow - new Date(this.birthday).getTime()) / 31556952000);
      return console.log(`User ${this.firstName} is ${userAge} years old.`);
    },
    getPassword() {
      let [year, ,] = this.birthday.split("-");
      return console.log(this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + year);
    },
  };
  Object.defineProperty(newUser, "firstName", {
    value: prompt("Please enter your name!", "Robby"),
    writable: false,
  });
  Object.defineProperty(newUser, "lastName", {
    value: prompt("Please enter your surname!", "Williams"),
    writable: false,
  });
  newUser.birthday = prompt('Please enter the date in format "dd.mm.yyyy":', "16.01.1995").split(".").reverse().join("-");

  return newUser;
}

let myUser = createNewUser();
console.log(myUser);

myUser.getAge();
myUser.getPassword();
