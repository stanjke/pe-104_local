// 1. Почему для работы с input не рекомендуется использовать события клавиатуры?

// Например, если пользователь скопирует текст в поле input из другого источника, например,
// через контекстное меню или сочетание клавиш Ctrl+C, то события клавиатуры не будут вызваны,
// и никакие обработчики не будут выполнены. Это может привести к тому, что данные в поле input
// не будут обновлены или обработаны в соответствии с требованиями приложения.

let btnWrap = document.querySelector(".btn-wrapper");
let buttons = document.querySelectorAll(".btn");

document.addEventListener("keypress", function (event) {
  pressedKey = event.key;
  for (btn of buttons) {
    if (btn.textContent.toLowerCase() === pressedKey.toLowerCase()) {
      if (btn.classList.contains("active")) {
        btn.classList.remove("active");
      } else {
        removeActive(buttons);
        btn.classList.add("active");
      }
    } else {
      continue;
    }
  }
});

function removeActive(arr) {
  arr.forEach((elem) => {
    if (elem.classList.contains("active")) elem.classList.remove("active");
  });
}
