// ТЕОРЕТИЧНІ ПИТАННЯ:

//  1.Опишіть своїми словами, що таке метод об'єкту
//  2.Який тип даних може мати значення властивості об'єкта?
//  3.Об'єкт це посилальний тип даних. Що означає це поняття?

//  1. Метод объекту это функция которая написана внутри объекта и которую можно вызывать у объекта с помощью такого синтаксиса object.method(), где object это
//  название вашего объекта а .method() название вашей функции которая написана внутри вашего объекта.
//  2. Значение у свойства объекта может быть любым типом данных.
//  3. На сколько я понял, что референсный тип - это когда под наш объект выделяется ячейка памяти на которую мы ссылаемся при обращениее. Главная особенность
//  это то, что если мы будем копировать наш объект, например, как преременную, ты мы всего навсего скопируем ссылку на наш объект, и любое взаимодействие с
//  нашей новой переменной будет менять наш изначальный объект. Как будето у нас есть комната с дверью и замком, вот объект это комнатая, а переменные которые ссылаются на него
//  это ключи от замка. Ключи разные а комната одна. По этому для того что бы скопировать объект нам нужно создать новый объект и в него скопировать значения из объекта который мы
//  изначально хотели скопировать.

// ЗАВДАННЯ

// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

// Необов'язкове завдання підвищеної складності

// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.

// FIRST PART:

// function createNewUser() {
//   const newUser = {
//     firstName: prompt("Please enter your name:"),
//     lastName: prompt("Please enter your surname:"),
//     getLogin() {
//       return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
//     },
//   };

//   console.log(newUser.getLogin());
//   //   newUser.firstName = prompt("Please enter your name:");
//   //   newUser.lastName = prompt("Please enter your surname:");
//   return newUser;
// }

// createNewUser();

//SECOND PART:

function createNewUser() {
  const newUser = {
    firstName: null,
    lastName: null,
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    setFirstName(value) {
      Object.defineProperty(this, "firstName", {
        writable: true,
      });
      this.firstName = value;
    },
    setLastName(value) {
      Object.defineProperty(this, "lastName", {
        writable: true,
      });
      this.lastName = value;
    },
  };

  Object.defineProperty(newUser, "firstName", {
    value: prompt("Please enter your name!", "Robby"),
    // configurable: true,
    writable: false,
  });

  Object.defineProperty(newUser, "lastName", {
    value: prompt("Please enter your surname!", "Williams"),
    // configurable: true,
    writable: false,
  });

  return newUser;
}

let myUser = createNewUser();
console.log(myUser);

myUser.firstName = "Jhon";
myUser.lastName = "Snow";
console.log(myUser);

myUser.setFirstName("Jhon");
myUser.setLastName("Snow");
console.log(myUser);

console.log(myUser.getLogin());
